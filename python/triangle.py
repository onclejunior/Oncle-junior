i = 1
x ='x'
while i < 11 :
    print('{}'.format(i*x))
    i = i+1
print('')    
print('joli triangle rectangle normal')
print('')

x ='x'
j = 11
while j > 0:
    print('{}'.format(j*x))
    j = j-1
print('')    
print('joli triangle rectangle inverse')

k = 1
l = 11
x ='x'
y =' '
while k < 11 :
    print('{}'.format(l*y) + '{}'.format(k*x))
    k = k+1
    l = l-1
print('')    
print('joli triangle rectangle gauche')
print('')

k = 1
l = 11
x ='x'
y =' '
while k < 11 :
    print('{}'.format(l*y) + '{}'.format(k*x) + '{}'.format((k-1)*x))
    k = k+1
    l = l-1
print('')    
print('joli triangle rectangle isocele')
print('')