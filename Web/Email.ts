interface Customer {
    name: string;
    email: string;
}

function generateEmail(customer: Customer): string {
    return `
        Hello ${customer.name},

        Thank you for being a valued customer. We appreciate your trust in us.

        Best regards,
        Your Company
    `;
    }

    const customer: Customer = { name: 'Lisa', email: 'lisa@example.com' };
    console.log(generateEmail(customer));