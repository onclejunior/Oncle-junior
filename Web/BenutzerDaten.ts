interface User {
    name: string;
    email: string;
    age: number;
    }

    function validateUser(user: any): user is User {
        return (
            typeof user.name === 'string' &&
            typeof user.email === 'string' &&
            typeof user.age === 'number'
        );
    }

    const input = { name: 'Max', email: 'max@example.com', age: 25 };

    if (validateUser(input)) {
        console.log('Valid user:', input);
    } else {
        console.error('Invalid user data');
}
