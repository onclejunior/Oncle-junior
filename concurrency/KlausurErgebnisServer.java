import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class KlausurErgebnisServer {
	public static void main(String args[]) {
		if (args.length < 1) {
			System.out.println("Error: Usage <RMI Port>");
			System.exit(0);
		}
		
		try {
			Registry registry = LocateRegistry.createRegistry(Integer.parseInt(args[0]));
			KlausurErgebnisImpl keImpl = new KlausurErgebnisImpl();
			KlausurErgebnis ke = (KlausurErgebnis) UnicastRemoteObject.exportObject(keImpl, 0);
			registry.bind("KlausurErgebnis", ke);
			System.out.println("Server bereit");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
