import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class StreetSymflowerTest {
	@Test
	public void Street4() {
		int n = 0;
		Street expected = new Street(0);
		Street actual = new Street(n);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void getNumber5() throws IllegalAccessException, NoSuchFieldException {
		Street s = new Street(0);
		final Field fieldFree = Street.class.getDeclaredField("free");
		fieldFree.setAccessible(true);
		fieldFree.set(s, false);
		int expected = 0;
		int actual = s.getNumber();

		assertEquals(expected, actual);
	}

	@Test
	public void getStreet6() throws IllegalAccessException, NoSuchFieldException {
		Street s = new Street(0);
		int c = 0;
		s.getStreet(c);

		Street sExpected = new Street(0);
		final Field fieldFree = Street.class.getDeclaredField("free");
		fieldFree.setAccessible(true);
		fieldFree.set(sExpected, false);

		assertTrue(EqualsBuilder.reflectionEquals(sExpected, s, false, null, true));
	}

	@Test
	public void releaseStreet7() {
		Street s = new Street(0);
		int c = 0;
		s.releaseStreet(c);
	}

	@Test
	public void releaseStreet8() throws IllegalAccessException, NoSuchFieldException {
		Street s = new Street(0);
		final Field fieldCarNumber = Street.class.getDeclaredField("carNumber");
		fieldCarNumber.setAccessible(true);
		fieldCarNumber.set(s, 65536);
		final Field fieldFree = Street.class.getDeclaredField("free");
		fieldFree.setAccessible(true);
		fieldFree.set(s, false);
		int c = 0;
		s.releaseStreet(c);
	}
}
