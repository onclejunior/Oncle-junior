import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SE3SimpleGui {

    private JFrame frame;
    private JButton btn;
    private JTextField txt;

    /*
     * Die Methode SetText
     * public static String setText(int text) {
     * System.out.println(text);
     * }
     */
    public static String setText(Object obj) {
        return (obj == null) ? "null" : obj.toString();
    }

    /* Klasse Thread Count */
    class Count extends Thread {

        private static int i;

        public static int getI() {
            return i;
        }

        Count() {
            super();
        }

        public void run() {
            i = 0;
            while (true) {
                i += 1;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    // Konstruktor von Count:
    public void Count() {
        Thread counter = new Count();
        counter.start();
    }

    /* Klasse Thread Paint */
    class Paint extends Thread {

        Paint() {
        }

        public void run() {
            while (true) {
                setText(Count.getI());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    // Konstruktor von Count:
    public void Paint() {
        Thread painter = new Paint();
        painter.start();
    }

    private void buttonActionPerformed() {
        if (String.valueOf(0) != "0") {
            ((JButton) btn).setText("Stop");
            System.out.println(btn.getText());
        } else if (String.valueOf(0) == "0") {
            ((JButton) btn).setText("Start");
            System.out.println(btn.getText());
        }
    }

    private void createAndShow() {
        /* links: ein Button mit Beschriftung */
        btn = new JButton();
        btn.setText("Run");

        /* Actionlistenner */
        btn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent actionEvent) {
                        buttonActionPerformed();
                    }
                });

        /* rechts: ein Textfeld */
        txt = new JTextField();

        /* Hauptfenster */
        frame = new JFrame();
        /* Aktion beim schließen: JVM beenden */
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /* Layout festlegen und Button & Textfeld hinzufügen */
        frame
                .getContentPane()
                .setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
        frame.getContentPane().add(btn);
        frame.getContentPane().add(txt);

        /* Größe setzen und anzeigen */
        frame.setPreferredSize(new Dimension(300, 100));
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        /* Im EDT ausführen: new SE3SimpleGui().createAndShow(); */
        javax.swing.SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        new SE3SimpleGui().createAndShow();
                    }
                });
    }
}