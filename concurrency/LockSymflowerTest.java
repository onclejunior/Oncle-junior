import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class LockSymflowerTest {
	@Test
	public void Lock1() {
		Lock expected = new Lock();
		Lock actual = new Lock();

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void lock2() throws IllegalAccessException, NoSuchFieldException, InterruptedException {
		Lock l = new Lock();
		final Field fieldNbrCtr = Lock.class.getDeclaredField("nbrCtr");
		fieldNbrCtr.setAccessible(true);
		fieldNbrCtr.set(l, -1);
		l.lock();

		Lock lExpected = new Lock();
		final Field fieldUnlocked = Lock.class.getDeclaredField("unlocked");
		fieldUnlocked.setAccessible(true);
		fieldUnlocked.set(lExpected, false);

		assertTrue(EqualsBuilder.reflectionEquals(lExpected, l, false, null, true));
	}

	@Test
	public void lock3() throws IllegalAccessException, NoSuchFieldException, InterruptedException {
		Lock l = new Lock();
		final Field fieldNbrCtr = Lock.class.getDeclaredField("nbrCtr");
		fieldNbrCtr.setAccessible(true);
		fieldNbrCtr.set(l, 2147483647);
		l.lock();

		Lock lExpected = new Lock();
		final Field fieldNbrCtr2 = Lock.class.getDeclaredField("nbrCtr");
		fieldNbrCtr2.setAccessible(true);
		fieldNbrCtr2.set(lExpected, -2147483648);
		final Field fieldUnlocked = Lock.class.getDeclaredField("unlocked");
		fieldUnlocked.setAccessible(true);
		fieldUnlocked.set(lExpected, false);

		assertTrue(EqualsBuilder.reflectionEquals(lExpected, l, false, null, true));
	}

	@Test
	public void unlock4() {
		Lock l = null; // TODO This is a fallback value due to incomplete analysis.
		l.unlock();
	}
}
