import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class ForkSymflowerTest {
	@Test
	public void Fork2() {
		int n = 0;
		Fork expected = new Fork(0);
		Fork actual = new Fork(n);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void get3() throws IllegalAccessException, NoSuchFieldException {
		Fork f = new Fork(0);
		int s = 0;
		f.get(s);

		Fork fExpected = new Fork(0);
		final Field fieldFree = Fork.class.getDeclaredField("free");
		fieldFree.setAccessible(true);
		fieldFree.set(fExpected, false);

		assertTrue(EqualsBuilder.reflectionEquals(fExpected, f, false, null, true));
	}

	@Test
	public void put4() throws IllegalAccessException, NoSuchFieldException {
		Fork f = new Fork(0);
		final Field fieldFree = Fork.class.getDeclaredField("free");
		fieldFree.setAccessible(true);
		fieldFree.set(f, false);
		final Field fieldSeat = Fork.class.getDeclaredField("seat");
		fieldSeat.setAccessible(true);
		fieldSeat.set(f, -2147483648);
		int s = 0;
		f.put(s);
	}

	@Test
	public void put5() {
		Fork f = new Fork(0);
		int s = 0;
		f.put(s);
	}
}
