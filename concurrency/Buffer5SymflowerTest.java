import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class Buffer5SymflowerTest {
	@Test
	public void Buffer51() throws IllegalAccessException, NoSuchFieldException {
		Buffer5 expected = new Buffer5();
		expected.lock = new Lock();
		final Field fieldOkToGet = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet.setAccessible(true);
		fieldOkToGet.set(expected, new Object());
		final Field fieldOkToPut = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut.setAccessible(true);
		fieldOkToPut.set(expected, new Object());
		Buffer5 actual = new Buffer5();

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void consumer5Consumer52() throws IllegalAccessException, NoSuchFieldException {
		Buffer5 b = null;
		Buffer5 outerClass2 = new Buffer5();
		outerClass2.lock = null;
		final Field fieldOkToGet2 = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet2.setAccessible(true);
		fieldOkToGet2.set(outerClass2, null);
		final Field fieldOkToPut2 = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut2.setAccessible(true);
		fieldOkToPut2.set(outerClass2, null);
		Buffer5.Consumer5 expected = outerClass2.new Consumer5(null);
		Buffer5 outerClass = new Buffer5();
		outerClass.lock = null;
		final Field fieldOkToGet = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet.setAccessible(true);
		fieldOkToGet.set(outerClass, null);
		final Field fieldOkToPut = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut.setAccessible(true);
		fieldOkToPut.set(outerClass, null);
		Buffer5.Consumer5 actual = outerClass.new Consumer5(b);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void consumer5Run3() throws IllegalAccessException, NoSuchFieldException {
		Buffer5 outerClass = new Buffer5();
		outerClass.lock = null;
		final Field fieldOkToGet = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet.setAccessible(true);
		fieldOkToGet.set(outerClass, null);
		final Field fieldOkToPut = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut.setAccessible(true);
		fieldOkToPut.set(outerClass, null);
		Buffer5.Consumer5 c = outerClass.new Consumer5(null);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		c.run();
		// });
	}

	@Test
	public void producer5Producer54() throws IllegalAccessException, NoSuchFieldException {
		int n = 0;
		Buffer5 b = null;
		Buffer5 outerClass2 = new Buffer5();
		outerClass2.lock = null;
		final Field fieldOkToGet2 = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet2.setAccessible(true);
		fieldOkToGet2.set(outerClass2, null);
		final Field fieldOkToPut2 = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut2.setAccessible(true);
		fieldOkToPut2.set(outerClass2, null);
		Buffer5.Producer5 expected = outerClass2.new Producer5(0, null);
		Buffer5 outerClass = new Buffer5();
		outerClass.lock = null;
		final Field fieldOkToGet = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet.setAccessible(true);
		fieldOkToGet.set(outerClass, null);
		final Field fieldOkToPut = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut.setAccessible(true);
		fieldOkToPut.set(outerClass, null);
		Buffer5.Producer5 actual = outerClass.new Producer5(n, b);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void producer5Run5() {
		Buffer5.Producer5 p = null; // TODO This is a fallback value due to incomplete analysis.
		p.run();
	}

	@Test
	public void get6() throws IllegalAccessException, NoSuchFieldException {
		Buffer5 b = new Buffer5();
		b.lock = null;
		final Field fieldOkToGet = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet.setAccessible(true);
		fieldOkToGet.set(b, null);
		final Field fieldOkToPut = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut.setAccessible(true);
		fieldOkToPut.set(b, null);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		b.get();
		// });
	}

	@Test
	public void put7() throws IllegalAccessException, NoSuchFieldException {
		Buffer5 b = new Buffer5();
		b.lock = null;
		final Field fieldOkToGet = Buffer5.class.getDeclaredField("okToGet");
		fieldOkToGet.setAccessible(true);
		fieldOkToGet.set(b, null);
		final Field fieldOkToPut = Buffer5.class.getDeclaredField("okToPut");
		fieldOkToPut.setAccessible(true);
		fieldOkToPut.set(b, null);
		int n = 0;
		// assertThrows(java.lang.NullPointerException.class, () -> {
		b.put(n);
		// });
	}
}
