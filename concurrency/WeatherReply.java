import java.io.Serializable;

public class WeatherReply implements Serializable {
	public final float temperature;
	public final float humidity;
	
	public WeatherReply(float temperature, float humidity) {
		this.temperature = temperature;
		this.humidity = humidity;
	}
}
