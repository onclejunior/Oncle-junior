import java.rmi.*;
import java.rmi.registry.*;

public class KlausurErgebnisClient {
	public static void main(String args[]) {
		if (args.length < 4) {
			System.out.println("Error: Usage <RMI host> <RMI port> <Matrikelnummer> <Passwort>");
			System.exit(0);
		}

		try {
			Registry registry = LocateRegistry.getRegistry(args[0], Integer.parseInt(args[1]));
			KlausurErgebnis ergebnis = (KlausurErgebnis) registry.lookup("KlausurErgebnis");
			double res = ergebnis.getErgebnis(Integer.parseInt(args[2]),args[3]);
			System.out.println("Ergebnis: " + res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
