import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class Philosopher3SymflowerTest {
	@Test
	public void Philosopher36() throws IllegalAccessException, NoSuchFieldException {
		int s = 0;
		Fork r = null;
		Fork l = null;
		Philosopher3 expected = new Philosopher3(0, null, null);
		final Field fieldLeftForkFirst = Philosopher3.class.getDeclaredField("leftForkFirst");
		fieldLeftForkFirst.setAccessible(true);
		fieldLeftForkFirst.set(expected, true);
		Philosopher3 actual = new Philosopher3(s, r, l);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void Philosopher37() throws IllegalAccessException, NoSuchFieldException {
		int s = 1;
		Fork r = null;
		Fork l = null;
		Philosopher3 expected = new Philosopher3(1, null, null);
		final Field fieldLeftForkFirst = Philosopher3.class.getDeclaredField("leftForkFirst");
		fieldLeftForkFirst.setAccessible(true);
		fieldLeftForkFirst.set(expected, false);
		Philosopher3 actual = new Philosopher3(s, r, l);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void run8() throws IllegalAccessException, NoSuchFieldException {
		Philosopher3 p = new Philosopher3(0, null, null);
		final Field fieldLeftForkFirst = Philosopher3.class.getDeclaredField("leftForkFirst");
		fieldLeftForkFirst.setAccessible(true);
		fieldLeftForkFirst.set(p, false);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		p.run();
		// });
	}

	@Test
	public void run9() throws IllegalAccessException, NoSuchFieldException {
		Philosopher3 p = new Philosopher3(0, null, null);
		final Field fieldLeftForkFirst = Philosopher3.class.getDeclaredField("leftForkFirst");
		fieldLeftForkFirst.setAccessible(true);
		fieldLeftForkFirst.set(p, true);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		p.run();
		// });
	}

	@Test
	public void run10() throws IllegalAccessException, NoSuchFieldException {
		Philosopher3 p = new Philosopher3(0, new Fork(0), null);
		final Field fieldLeftForkFirst = Philosopher3.class.getDeclaredField("leftForkFirst");
		fieldLeftForkFirst.setAccessible(true);
		fieldLeftForkFirst.set(p, false);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		p.run();
		// });
	}

	@Test
	public void run11() throws IllegalAccessException, NoSuchFieldException {
		Philosopher3 p = new Philosopher3(0, null, new Fork(0));
		final Field fieldLeftForkFirst = Philosopher3.class.getDeclaredField("leftForkFirst");
		fieldLeftForkFirst.setAccessible(true);
		fieldLeftForkFirst.set(p, true);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		p.run();
		// });
	}
}
