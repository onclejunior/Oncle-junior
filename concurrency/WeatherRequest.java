import java.io.Serializable;

public class WeatherRequest implements Serializable {
	private int zipCode;
	private String town;
	
	public WeatherRequest(int zipCode) {
		this.zipCode = zipCode;
	}
	
	public WeatherRequest(String town) {
		this.town = town;
	}
	
	public int getZipCode() {
		return zipCode;
	}
	
	public String getTown() {
		return town;
	}
}
