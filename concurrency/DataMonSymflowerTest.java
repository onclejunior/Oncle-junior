import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class DataMonSymflowerTest {
	@Test
	public void getChangeCount1() {
		DataMon d = new DataMon();
		int expected = 0;
		int actual = d.getChangeCount();

		assertEquals(expected, actual);
	}

	@Test
	public void getData2() {
		DataMon d = new DataMon();
		int expected = 0;
		int actual = d.getData();

		assertEquals(expected, actual);
	}

	@Test
	public void setData3() {
		DataMon d = null; // TODO This is a fallback value due to incomplete analysis.
		int data = 0; // TODO This is a fallback value due to incomplete analysis.
		d.setData(data);
	}

	@Test
	public void waitForChange4() throws InterruptedException {
		DataMon d = new DataMon();
		d.waitForChange();
	}
}
