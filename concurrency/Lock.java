/** Einfache Sperre (Mutual Exclusive Lock). */
public class Lock {
	/** Lockzustand. Gegen nebenläufige Zugriffe durch Objektsperre geschützt. */
	private boolean locked = false;

	/** Lock anfordern.
	 * Wartet auf den Lock bis er von keinem Thread mehr gehalten wird.
	 * <p>Anmerkung: Falls ein Thread mehrmals @lock() aufruft, ohne dazwischen @unlock() aufzurufen, ist das Verhalten unspezifiziert.</p>
	 * @throws InterruptedException falls der Thread unterbrochen wurde, während er auf den Lock gewartet hat
	 */
	public synchronized void lock() throws InterruptedException {
		/* warten, bis Lock frei ist */
		while (locked)
			wait();
		/* Lock setzen */
		locked = true;
	}

	/** Lock freigeben.
	 * Gibt den Lock frei, der vorher durch den aufrufenden Thread mittels @lock() angefordert wurde.
	 * <p>Anmerkung: Wenn ein thread @unlock() aufruft, ohne den Lock zu halten, ist das Verhalten unspezifiziert</p>
	 */
	public synchronized void unlock() {
		/* Lock freigeben */
		locked = false;
		/* einen anderen Thread benachrichtigen */
		notify();
	}
}
