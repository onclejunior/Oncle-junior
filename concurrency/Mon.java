class DataMon {

  private int data = 0;
  private volatile int count = 0;

  public synchronized int getData() {
    return data;
  }

  public synchronized void setData(int data) {
    this.data = data;
    count++;

    notifyAll();
  }

  public synchronized int getChangeCount() {
    return count;
  }

  public synchronized void waitForChange() throws InterruptedException {
    while (data != getData()) {
      System.out.println("   wait\r");
      wait();
    }
  }

}

class monit extends Thread {

  private DataMon dataMonitor;

  monit(String str, DataMon dtm) {
    super(str);
    dataMonitor = dtm;
  }

  public void run() {
    for (int i = 0; i < 5; i++) {
      dataMonitor.setData(1);
      System.out.println(getName() + "; count = " + dataMonitor.getChangeCount() + "; data = " + dataMonitor.getData());

    }
  }
}

public class Mon {

  static DataMon date = null;

  public static void main(String[] args) {
    int client = 5;
    date = new DataMon();
    for (int j = 0; j < client; j++) {
      new monit("Customer 11 " + (j + 1), date).start();
      new monit("Customer 22 " + (j + 1), date).start();
    }
  }
}