
public class FairLock {
  private boolean locked = false;
  private Thread lockThread = null;
  private List<QueueObject> waitingThreads = new ArrayList<QueueObject>();

  public synchronized void lock() throws InterruptedException {
    while (locked) {
      wait();
    }
    locked = true;
    lockThread = Thread.currentThread();
  }

  public synchronized void unlock() {
    if (this.lockThread != Thread.currentThread()) {
      throw new IllegalStateException("Calling Thread has not called this lock");
    }
    locked = false;
    lockThread = null;
    notify();

  }
}

/*
 * class Lock {
 * private volatile boolean unlocked;
 * Lock() {
 * unlocked = true;
 * }
 * 
 * public synchronized void lock() throws InterruptedException {
 * while (!unlocked) {
 * System.out.println(" You must wait!");
 * wait();
 * }
 * unlocked = false;
 * System.out.println(" You have the Lock " );
 * }
 * 
 * public synchronized void unlock() {
 * unlocked = true;
 * System.out.println(" A new Thread can have the Lock ");
 * notifyAll();
 * }
 * }
 */
