import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class MultiThreadedSymflowerTest {
	@Test
	public void Dumper1() {
		MultiThreaded use = null;
		Dumper expected = new Dumper(null);
		Dumper actual = new Dumper(use);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void run2() {
		Dumper d = new Dumper(null);
		d.is_active = false;
		d.run();
	}

	@Test // (expected = java.lang.NullPointerException.class)
	public void run3() {
		Dumper d = new Dumper(null);
		d.run();
	}

	@Test
	public void evaluateInput4() {
		MultiThreaded m = null; // TODO This is a fallback value due to incomplete analysis.
		Dumper used_dumper = null; // TODO This is a fallback value due to incomplete analysis.
		m.evaluateInput(used_dumper);
	}

	@Test
	public void main5() {
		String[] args = null; // TODO This is a fallback value due to incomplete analysis.
		MultiThreaded.main(args);
	}
}
