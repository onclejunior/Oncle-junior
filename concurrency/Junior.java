public class FairLock {
    private boolean locked = false;
    private Thread lockThread = null;
    // eine Liste der wartenden Threads
    private List<QueueObject> waitingThreads = new ArrayList<QueueObject>();

    // nur die notwendigen Blocks werden synchronisiert
    public void lock() throws InterruptedException {
        // FairLock erstellt eine neue Instanz von QueueObject und fügt sie für jeden
        // Thread ein,
        // der lock() aufruft, in die Warteschlange
        QueueObject queueObject = new QueueObject();
        boolean isLockedForThisThread = true;
        synchronized (this) {
            waitingThreads.add(queueObject);
        }

        while (isLockedForThisThread) {
            synchronized (this) {
                isLockedForThisThread = locked || waitingThreads.get(0) != queueObject;
                if (!isLockedForThisThread) {
                    locked = true;
                    waitingThreads.remove(queueObject);
                    lockThread = Thread.currentThread();
                    return;
                }
            }
            try {
                queueObject.doWait();
            } catch (InterruptedException e) {
                synchronized (this) {
                    waitingThreads.remove(queueObject);
                }
                throw e;
            }
        }
    }

    public synchronized void unlock() {
        if (this.lockThread != Thread.currentThread()) {
            throw new IllegalMonitorStateException(
                    "Calling thread has not locked this lock");
        }
        locked = false;
        lockThread = null;
        // wird nur ein wartender Thread gleichzeitig aufgeweckt,
        // anstatt alle wartenden Threads. Dieser Teil regelt die Fairness des FairLock.
        if (waitingThreads.size() > 0) {
            waitingThreads.get(0).doNotify();
        }
    }
}

public class QueueObject {

    private boolean isNotified = false;

    public synchronized void doWait() throws InterruptedException {
        while (!isNotified) {
            this.wait();
        }
        this.isNotified = false;
    }

    public synchronized void doNotify() {
        this.isNotified = true;
        this.notify();
    }

    public boolean equals(Object o) {
        return this == o;
    }
}