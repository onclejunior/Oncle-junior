import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class AddCallable implements Callable<BigInteger> {

  private int add;

  // Ein Konstruktor der Klasse AddCallable
  AddCallable(int add) {
    this.add = add;
  }

  /*
   * Die Funktion die alle positiven ganzen Zahlen
   * bis zum übergebenen Wert addiert.
   */

  public static BigInteger addit(int f) {
    if (f == 0) {
      return BigInteger.ZERO;
    } else {
      return BigInteger.valueOf(f).add(addit(f - 1));
    }
  }

  public BigInteger call() {
    return addit(add);
  }
}

class MultiCallable implements Callable<BigInteger> {

  private int mul;

  // Ein Konstruktor der KLasse MultiCallable

  MultiCallable(int mul) {
    this.mul = mul;
  }

  /*
   * Die Funktion die alle positiven ganzen Zahlen
   * bis zum übergebenen Wert multiplitiert (Fakultät).
   */

  public static BigInteger fac(int f) {
    if (f <= 1) {
      return BigInteger.valueOf(1);
    } else {
      return BigInteger.valueOf(f).multiply(fac(f - 1));
    }
  }

  public BigInteger call() {
    return fac(mul);
  }
}

public class AddMultiCallable {

  public static void main(String[] args) {
    // Erzeugung von zwei Callable-Objekte
    Callable<BigInteger> CC = new AddCallable(1000);
    Callable<BigInteger> CC2 = new MultiCallable(1000);

    // Erzeugung von einem ScheduledThreadPoolExecutor
    ExecutorService executor = Executors.newScheduledThreadPool(1);
    //
    Future<BigInteger> future = executor.submit(CC);
    Future<BigInteger> future2 = executor.submit(CC2);
    try {
      BigInteger resultAddCallable = future.get();
      BigInteger resultMultiCallable = future2.get();
      System.out.println("MultiCallable von 1000 ist: " + resultMultiCallable);
      System.out.println("AddCallable von 1000 ist: " + resultAddCallable);
    } catch (InterruptedException | ExecutionException e) {}
    executor.shutdown();
  }
}
