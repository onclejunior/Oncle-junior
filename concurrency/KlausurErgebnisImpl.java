public class KlausurErgebnisImpl implements KlausurErgebnis {
	public double getErgebnis(int matrikelNr, String userPwd) {
		if (checkPwd(matrikelNr, userPwd))
			return queryErgebnis(matrikelNr);
		else return -1;
	}

	public boolean setErgebnis(String masterPwd, int matrikelNr, double ergebnis) {
		if (checkMasterPwd(masterPwd)) {
			insertErgebnis(matrikelNr, ergebnis);
			return true;
		}
		else return false;
	}

	
	private boolean checkPwd(int matrikelNr, String userPwd) {
		// do some password checking
		return true;
	}

	private boolean checkMasterPwd(String masterPwd) {
		// do some password checking
		return true;
	}

	private double queryErgebnis(int matrikelNr) {
		return 2.0;
	}

	private void insertErgebnis(int matrikelNr, double ergebnis) {
		// insert data
	}
}
