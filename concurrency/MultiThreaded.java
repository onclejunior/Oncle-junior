public class MultiThreaded {
	volatile int status = 0;

	public static void main(String[] args) {

		/*
		 * Wir instanziieren MultiThread und Dumper, ohne die relevanten Funktionen zu
		 * starten. Da Dumper auf ein MultiThreaded-
		 * Objekt verweist, muss dieses zuerst instantiiert werden. Sobald der Dumper
		 * danach instantiiert und gestartet wurde, können
		 * wir "evaluateInput" aufrufen.
		 */

		System.out.print("BEGIN");
		MultiThreaded multi_thread_instance = new MultiThreaded();
		Dumper dumper_instance = new Dumper(multi_thread_instance);
		dumper_instance.start();
		multi_thread_instance.evaluateInput(dumper_instance);
		System.out.print("END");
	}

	// Wir übergeben "evaluateInput" das genutzte Dumper-Objekt
	public void evaluateInput(Dumper used_dumper) {
		int input;

		while (true) {
			try {
				input = System.in.read();
			} catch (Exception e) {
				input = 0;
			}

			switch (input) {
				case '1':
				case '2':
				case '3':
					status = input - '0';
					break;
				case 'q':
					// Wir setzen "is_active" im genutzten Dumper auf false, um den Dumper zu
					// stoppen
					used_dumper.is_active = false;
					return;
				default:
			}
		}
	}
} // class MultiThreaded

class Dumper extends Thread {

	MultiThreaded x;

	/*
	 * "is_active" gibt an, ob die while Schleife in "run" abgebrochen werden soll.
	 * Der assoziierte "MultiThreaded" hat Zugriff auf "is_active" und kann damit
	 * "run" beenden
	 */
	boolean is_active = true;

	public Dumper(MultiThreaded use) {
		x = use;
	}

	public void run() {
		while (true && is_active == true) {
			System.out.println(x.status);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
		}
	}

}
