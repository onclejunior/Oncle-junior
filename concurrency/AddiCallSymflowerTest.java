import java.math.BigInteger;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class AddiCallSymflowerTest {
	@Test
	public void AddCallable1() {
		int add = 0;
		AddCallable expected = new AddCallable(0);
		AddCallable actual = new AddCallable(add);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void addit2() {
		int f = 0; // TODO This is a fallback value due to incomplete analysis.
		BigInteger expected = null; // TODO This is a fallback value due to incomplete analysis.
		BigInteger actual = AddCallable.addit(f);

		assertEquals(expected, actual);
	}

	@Test
	public void call3() {
		AddCallable a = null; // TODO This is a fallback value due to incomplete analysis.
		BigInteger expected = null; // TODO This is a fallback value due to incomplete analysis.
		BigInteger actual = a.call();

		assertEquals(expected, actual);
	}

	@Test
	public void MultiCallable4() {
		int mul = 0;
		MultiCallable expected = new MultiCallable(0);
		MultiCallable actual = new MultiCallable(mul);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void call5() {
		MultiCallable m = null; // TODO This is a fallback value due to incomplete analysis.
		BigInteger expected = null; // TODO This is a fallback value due to incomplete analysis.
		BigInteger actual = m.call();

		assertEquals(expected, actual);
	}

	@Test
	public void fac6() {
		int f = 0; // TODO This is a fallback value due to incomplete analysis.
		BigInteger expected = null; // TODO This is a fallback value due to incomplete analysis.
		BigInteger actual = MultiCallable.fac(f);

		assertEquals(expected, actual);
	}
}
