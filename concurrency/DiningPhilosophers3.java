public class DiningPhilosophers3 {
    public static void main(String[] args) {
        Fork fork0 = new Fork(0);
        Fork fork1 = new Fork(1);
        Fork fork2 = new Fork(2);
        Fork fork3 = new Fork(3);
        new Philosopher3(0, fork3, fork0).start();
        new Philosopher3(1, fork0, fork1).start();
        new Philosopher3(2, fork1, fork2).start();
        new Philosopher3(3, fork2, fork3).start();
    }
}

class Fork {

    private int number;
    private int seat;
    private boolean free = true;

    public Fork(int n) {
        number = n;
    }

    public synchronized void get(int s) {
        while (!free) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        free = false;
        seat = s;
        System.out.println("Philosopher " + seat + " gets fork " + number);
    }

    public synchronized void put(int s) {
        if (seat == s && !free) { // put only after get; same philosopher
            free = true;
            System.out.println("Philosopher " + seat + " puts fork " + number);
            notifyAll();
        }
    }
}

class Philosopher3 extends Thread {
    private Fork leftFork, rightFork;
    private int seat;
    // gibt an, ob der Philosoph zuerst die linke oder zuerst die rechte Gabel
    // nimmt (aendert sich bei jedem Durchlauf)
    private boolean leftForkFirst;

    Philosopher3(int s, Fork r, Fork l) {
        seat = s;
        leftForkFirst = (seat % 2 == 0);
        leftFork = l;
        rightFork = r;
    }

    @Override
    public void run() {
        while (true) {
            think();
            if (leftForkFirst) {
                leftFork.get(seat);
                rightFork.get(seat);
                eat();
                rightFork.put(seat);
                leftFork.put(seat);
            } else {
                rightFork.get(seat);
                leftFork.get(seat);
                eat();
                leftFork.put(seat);
                rightFork.put(seat);
            }
            // Reihenfolge der Ressourcenanforderung wird im naechsten

            // Durchlauf umgekehrt
            leftForkFirst = !leftForkFirst;
        }
    }

    private void think() {
        System.out.println("Philosopher " + seat + " thinking");
    }

    private void eat() {
        System.out.println("Philosopher " + seat + " eating");
    }
}