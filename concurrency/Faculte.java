import java.math.BigInteger;

public class Faculte {

    public static BigInteger fac(int f) {
        if (f <= 1) {
            return BigInteger.valueOf(1);
        } else {
            return BigInteger.valueOf(f).multiply(fac(f - 1));
        }
    }

    public static void main(String[] args) {
        System.out.println(fac(1000));
    }
}