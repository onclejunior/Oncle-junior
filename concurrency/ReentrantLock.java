/** Einfache reentrante Sperre (Mutual Exclusive Lock). */
public class ReentrantLock {
	/** Locktiefe: wie oft hält der gleiche Thread den Lock */
	private int locked = 0;
	/** Referenz auf Thread, der den Lock hält oder @null falls Lock nicht gesperrt */
	private Thread t = null;

	/** Lock anfordern.
	 * Wartet auf den Lock bis er von keinem anderen Thread mehr gehalten wird.
	 * @throws InterruptedException falls der Thread unterbrochen wurde, während er auf den Lock gewartet hat
	 */
	public synchronized void lock() throws InterruptedException {
		if (locked != 0 && t == Thread.currentThread()) {
			/* Lock wird von diesem Thread gehalten -> Locktiefe erhöhen */
			/* Anmerkung: in einer besseren Implementierung sollte ein Überlauf dieser Variable als Exception angezeigt werden */
			++locked;
		} else {
			/* Lock wird nicht oder nicht von diesem Thread gehalten */

			/* warten, bis Lock frei ist */
			while (locked != 0)
				wait();
			/* Lock setzen */
			t = Thread.currentThread();
			locked = 1;
		}
	}

	/** Lock freigeben.
	 * Gibt den lock frei, der vorher durch den aufrufenden Thread mittels @lock() angefordert wurde.
	 * <p>Anmerkung: Wenn ein thread @unlock() aufruft, ohne den Lock zu halten, ist das Verhalten unspezifiziert</p>
	 */
	public synchronized void unlock() {
		/* Verbesserung: aufrufenden Thread prüfen */
		if (t != Thread.currentThread()) {
			if (t != null) /* Freigabe durch falschen Thread */
				throw new IllegalMonitorStateException("Lock was locked by thread " + t.toString() + ", but thread " +
						Thread.currentThread().toString() + " tried to unlock it");
			else /* Freigabe, obwohl nicht gesperrt*/
				throw new IllegalMonitorStateException("Lock was not locked, but thread " +
						Thread.currentThread().toString() + " tried to unlock it");
		}

		/* Locktiefe verringern */
		--locked;
		if (locked == 0) {
			/* Lock wieder freigegeben */
			t = null;
			/* einen anderen Thread aufwecken */
			notify();
		}
	}
}
