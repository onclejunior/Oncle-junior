<?php

namespace App\Controller;

use App\Entity\Karte;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\KarteType;

class KarteController extends AbstractController
{
    #[Route('/karte', name: 'karte_index', methods: ['GET', 'POST'])]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        // Neues Karten-Objekt erstellen
        $karte = new Karte();
        
        // Formular für die Karte erstellen
        $form = $this->createForm(KarteType::class, $karte);
        $form->handleRequest($request);

        // Wenn das Formular abgesendet wurde und gültig ist
        if ($form->isSubmitted() && $form->isValid()) {
            // Karte speichern
            $entityManager->persist($karte);
            $entityManager->flush();

            // Weiterleitung, um doppeltes Senden zu vermeiden
            return $this->redirectToRoute('karte_index');
        }

        // Hole alle Karten aus der Datenbank
        $karten = $entityManager->getRepository(Karte::class)->findAll();

        // Render die Ansicht
        return $this->render('karte/index.html.twig', [
            'karten' => $karten,
            'form' => $form->createView(), // Formular an das Template übergeben
            ]);
    }


    #[Route('/karte/new', name: 'karte_create', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        $karte = new Karte();
        
        // Erstelle das Formular
        $form = $this->createForm(KarteType::class, $karte);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($karte);
            $entityManager->flush();
            
            return $this->redirectToRoute('karte_index');
        }
        
        // Render das Formular
        return $this->render('karte/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/karte/{id}/edit', name: 'karte_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Karte $karte, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(KarteType::class, $karte);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            
            return $this->redirectToRoute('karte_index');
        }
        
        return $this->render('karte/edit.html.twig', [
            'form' => $form->createView(),
            'karte' => $karte,
        ]);
    }

    #[Route('/karte/{id}/delete', name: 'karte_delete', methods: ['GET'])]
    public function delete(Karte $karte, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($karte);
        $entityManager->flush();
        return $this->redirectToRoute('karte_index');
    }

}
