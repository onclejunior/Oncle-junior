<?php

namespace App\Form;

use App\Entity\Karte;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KarteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('karte1', TextType::class, ['label' => 'Karte 1 Überschrift'])
            ->add('karte1Unterpunkte', TextareaType::class, [
                'label' => 'Karte 1 Unterpunkte',
                'required' => false,
            ])
            ->add('karte2', TextType::class, ['label' => 'Karte 2 Überschrift'])
            ->add('karte2Unterpunkte', TextareaType::class, [
                'label' => 'Karte 2 Unterpunkte',
                'required' => false,
            ])
            ->add('karte3', TextType::class, ['label' => 'Karte 3 Überschrift'])
            ->add('karte3Unterpunkte', TextareaType::class, [
                'label' => 'Karte 3 Unterpunkte',
                'required' => false,
            ])
            ->add('karte4', TextType::class, ['label' => 'Karte 4 Überschrift'])
            ->add('karte4Unterpunkte', TextareaType::class, [
                'label' => 'Karte 4 Unterpunkte',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Karte::class,
        ]);
    }
}


