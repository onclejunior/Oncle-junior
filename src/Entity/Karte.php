<?php

namespace App\Entity;

use App\Repository\KarteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: KarteRepository::class)]
class Karte
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $karte1 = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $karte1Unterpunkte = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $karte2 = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $karte2Unterpunkte = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $karte3 = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $karte3Unterpunkte = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $karte4 = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $karte4Unterpunkte = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKarte1(): ?string
    {
        return $this->karte1;
    }

    public function setKarte1(string $karte1): self
    {
        $this->karte1 = $karte1;

        return $this;
    }

    public function getKarte1Unterpunkte(): ?string
    {
        return $this->karte1Unterpunkte;
    }

    public function setKarte1Unterpunkte(?string $karte1Unterpunkte): self
    {
        $this->karte1Unterpunkte = $karte1Unterpunkte;

        return $this;
    }

    public function getKarte2(): ?string
    {
        return $this->karte2;
    }

    public function setKarte2(string $karte2): self
    {
        $this->karte2 = $karte2;

        return $this;
    }

    public function getKarte2Unterpunkte(): ?string
    {
        return $this->karte2Unterpunkte;
    }

    public function setKarte2Unterpunkte(?string $karte2Unterpunkte): self
    {
        $this->karte2Unterpunkte = $karte2Unterpunkte;

        return $this;
    }

    public function getKarte3(): ?string
    {
        return $this->karte3;
    }

    public function setKarte3(string $karte3): self
    {
        $this->karte3 = $karte3;

        return $this;
    }

    public function getKarte3Unterpunkte(): ?string
    {
        return $this->karte3Unterpunkte;
    }

    public function setKarte3Unterpunkte(?string $karte3Unterpunkte): self
    {
        $this->karte3Unterpunkte = $karte3Unterpunkte;

        return $this;
    }

    public function getKarte4(): ?string
    {
        return $this->karte4;
    }

    public function setKarte4(string $karte4): self
    {
        $this->karte4 = $karte4;

        return $this;
    }

    public function getKarte4Unterpunkte(): ?string
    {
        return $this->karte4Unterpunkte;
    }

    public function setKarte4Unterpunkte(?string $karte4Unterpunkte): self
    {
        $this->karte4Unterpunkte = $karte4Unterpunkte;

        return $this;
    }
}

