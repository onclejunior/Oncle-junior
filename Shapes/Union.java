import java.util.ArrayList;

public class Union implements Shape {

  public ArrayList<Shape> shapes;

  public Union(ArrayList<Shape> shapes) {
    this.shapes = shapes;
  }

  @Override
  public boolean contains(int x, int y) {
    for (int i = 0; i < shapes.size(); i++) {
      if (shapes.get(i).contains(x, y)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public int rightmost() {
    int maxRight = 0;
    for (int i = 0; i < shapes.size(); i++) {
      if (shapes.get(i).rightmost() >= maxRight) {
        maxRight = shapes.get(i).rightmost();
      }
    }
    return maxRight;
  }

  @Override
  public int topmost() {
    int maxTop = 0;
    for (int i = 0; i < shapes.size(); i++) {
      if (shapes.get(i).topmost() >= maxTop) {
        maxTop = shapes.get(i).topmost();
      }
    }
    return maxTop;
  }
}
