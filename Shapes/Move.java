public class Move implements Shape {

  private int moveX;
  private int moveY;
  private Shape shape;

  Move(Shape shape, int moveX, int moveY) {
    this.shape = shape;
    this.moveX = moveX;
    this.moveY = moveY;
  }

  @Override
  public boolean contains(int x, int y) {
    return shape.contains(x - moveX, y - moveY);
  }

  @Override
  public int rightmost() {
    return shape.rightmost() + moveX;
  }

  @Override
  public int topmost() {
    return shape.topmost() + moveY;
  }
}
