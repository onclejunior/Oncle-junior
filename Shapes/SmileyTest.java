import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class SmileyTest {
    private static Shape smiley;
    private static final Set<Entry<Integer, Integer>> smileyCoords = new HashSet<>();

    @BeforeAll
    static void getSmiley() {
        smiley = Smiley.getSmiley();
    }

    @BeforeAll
    static void calculateCoords() {
        IntStream.concat(IntStream.rangeClosed(1, 5), IntStream.rangeClosed(18, 22)).forEach(x -> {
            for (int y = 9; y <= 13; y++) {
                smileyCoords.add(new AbstractMap.SimpleImmutableEntry<>(x, y));
            }
        });
        for (int x = 11; x <= 12; x++) {
            for (int y = 6; y <= 11; y++) {
                smileyCoords.add(new AbstractMap.SimpleImmutableEntry<>(x, y));
            }
        }
        IntStream.concat(IntStream.rangeClosed(2, 4), IntStream.rangeClosed(19, 21)).forEach(x -> {
            for (int y = 3; y <= 5; y++) {
                smileyCoords.add(new AbstractMap.SimpleImmutableEntry<>(x, y));
            }
        });
        for (int x = 3; x <= 20; x++) {
            for (int y = 1; y <= 3; y++) {
                smileyCoords.add(new AbstractMap.SimpleImmutableEntry<>(x, y));
            }
        }
    }

    @Test
    void smileyRightmost() {
        assertEquals(22, smiley.rightmost());
    }

    @Test
    void smileyTopmost() {
        assertEquals(13, smiley.topmost());
    }

    private static Stream<Arguments> getCoordsToTest() {
        return IntStream.range(-5, 30).boxed().flatMap(x ->
                IntStream.range(-5, 30).mapToObj(y -> Arguments.of(x, y))
        );
    }

    @ParameterizedTest
    @MethodSource("getCoordsToTest")
    void smileyContains(int x, int y) {
        assertEquals(
                smileyCoords.contains(new AbstractMap.SimpleImmutableEntry<>(x, y)),
                smiley.contains(x, y),
                "Das Ergebnis von contains(" + x + ", " + y + ") ist falsch!"
        );
    }
}
