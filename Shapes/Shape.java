public interface Shape {
    boolean contains(int x, int y);
    int rightmost();
    int topmost();
}
