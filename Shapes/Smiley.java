import java.util.ArrayList;

public class Smiley {

  public static Shape getSmiley() {
    Square bigSquare = new Square(4, 4);
    Square smallSquare = new Square(2, 2);
    Rectangle bigRectangle = new Rectangle(17, 2);
    Rectangle smallRectangle = new Rectangle(1, 5);

    Move bigSqMove1 = new Move(bigSquare, 1, 9);
    Move bigSqMove2 = new Move(bigSquare, 18, 9);
    Move smallSqMove1 = new Move(smallSquare, 2, 3);
    Move smallSqMove2 = new Move(smallSquare, 19, 3);
    Move smallRecMove = new Move(smallRectangle, 11, 6);
    Move bigRecMove = new Move(bigRectangle, 3, 1);
    ArrayList<Shape> shapes = new ArrayList<Shape>();
    shapes.add(bigSqMove1);
    shapes.add(bigSqMove2);
    shapes.add(smallSqMove1);
    shapes.add(smallSqMove2);
    shapes.add(smallRecMove);
    shapes.add(bigRecMove);
    Union smiley = new Union(shapes);
    return smiley;
  }
}
