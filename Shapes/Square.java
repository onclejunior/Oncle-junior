public class Square implements Shape {

  private int lange;
  private int breite;

  Square(int lange, int breite) {
    this.lange = lange;
    this.breite = breite;
  }

  @Override
  public boolean contains(int x, int y) {
    if (((x >= 0) && (x <= lange)) && ((y >= 0) && (y <= breite))) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public int rightmost() {
    return lange;
  }

  @Override
  public int topmost() {
    return breite;
  }
}
