public class Fille extends Mere {
    private final int y;

    public Fille(int x, int y) {
        super(x);
        this.y = y;
    }

    public int getY() {
        return y;
    }

    void Fille(int y) {
        super.Mere(x);
        System.out.println(" et je suis y = " + y);
    }

    public static void main(String[] args) {
        Fille f = new Fille(1, 3);
        f.Fille(3);
    }
}