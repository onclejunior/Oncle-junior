import java.util.ArrayList;

public class Miroir {
    public static void main(String[] args) {
        ArrayList<Integer> alpha = new ArrayList<Integer>();
        alpha.add(10);
        alpha.add(25);
        alpha.add(6);
        alpha.add(1);
        alpha.add(8);
        alpha.add(7);
        alpha.add(14);
        alpha.add(20);
        alpha.add(12);
        alpha.add(37);

        int i = 0;
        int s = alpha.size() - 1;
        while ((i < s) && (s > 0)) {
            int tmp = alpha.get(i);
            int tmp2 = alpha.get(s);
            alpha.set(i, tmp2);
            alpha.set(s, tmp);
            ++i;
            --s;
        }
        System.out.println(alpha);
    }
}
