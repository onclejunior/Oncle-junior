import java.util.Scanner;

public class kaprekar {

	static int expo(int n, int p) {
		if (p == 1) {
			return n;
		} else {
			return (n * expo(n, p - 1));
		}
	}

	static int sommeParties(int n, int p) {
		return (n / expo(10, p) + (n % (expo(10, p))));
	}

	public static void main(String[] args) {
		System.out.println("Entrez un nombre et un chiffre! ");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int p = sc.nextInt();
		sc.close();
		int m = n * n;
		if (sommeParties(m, p) == n) {
			System.out.print(true);
		} else {
			System.out.print(false);
		}
	}
}
