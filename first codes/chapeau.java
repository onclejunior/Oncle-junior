import java.util.Scanner;

public class chapeau {
    public static void affichecaractere(char c) {
        System.out.print("*");
    }

    public static void ligneSansReturn(int n, char c) {
        while (1 < n) {
            System.out.print("*" + " ");
            --n;
        }
        System.out.print("*");
    }

    public static void unCaractereSansReturn(int n, char c) {
        int a = 0;
        while (a != n) {
            System.out.print("*");
            ++a;
        }
    }

    public static void unCaractereAvecReturn(int n, char c) {
        System.out.println(" *");
    }

    public static void espaces(int n) {
        while (2 <= n) {
            System.out.print("  ");
            --n;
        }
    }

    public static void ligneAvecReturn(int n, char c) {
        while (1 < n) {
            System.out.print("* ");
            --n;
        }
        System.out.println("*");
    }

    public static void carre(int n) {
        int i = 1;
        ligneAvecReturn(n, '*');
        while ((i >= 1) && (i < n - 1)) {
            unCaractereSansReturn(1, '*');
            espaces(n - 1);
            unCaractereAvecReturn(n, '*');
            ++i;
        }
        ligneSansReturn(n, '*');
    }

    public static void chap(int centre, char c) {
        int i = 1;
        while (1 < centre) {
            espaces(++i);
            unCaractereAvecReturn(centre + 1, '*');
            centre += 1;
            ++i;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int taille;
        System.out.println("Saisissez la taille des figures : ");
        taille = scanner.nextInt();
        scanner.close();
        chap(taille, '*');
    }
}