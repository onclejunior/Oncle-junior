import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class EcoAno {

    public EcoAno() {
        JFrame Disc = new JFrame();
        ArrayList<JButton> Liste = new ArrayList<JButton>();
        Disc.setTitle("Gestionnaire de Disque Dur");
        Disc.setDefaultCloseOperation(Disc.EXIT_ON_CLOSE);
        Disc.getContentPane().setLayout(new FlowLayout());
        Liste.add(new JButton("Formater le disque dur!"));
        for (JButton disc : Liste) {
            Disc.getContentPane().add(disc);
            Liste.get(0).addActionListener(
                    (e) -> {
                        System.out.println("Formatage en cours!...");
                    });
        }
        Disc.setVisible(true);
        Disc.pack();

    }

    public static void main(String[] args) {
        new EcoAno();
    }
}