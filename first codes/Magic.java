public class Magic {

  public static int magicRecursive(int n) {
    if (n <= 0) {
      return 0;
    }
    int recursiveResult = magicRecursive(n - 1);
    if ((n % 5 == 0) || (n % 7 == 0)) {
      return recursiveResult + n;
    } else {
      return recursiveResult;
    }
  }

  // a)
  public static int magicFor(int n) {
    int result = 0;
    for (int i = 0; i <= n; i++) {
      if ((i % 5 == 0) || (i % 7 == 0)) {
        result += i;
      }
    }
    return result;
  }

  // b)
  public static int magicWhile(int n) {
    int results = 0;
    int i = 0;
    while (i <= n) {
      if ((i % 5 == 0) || (i % 7 == 0)) {
        results += i;
      }
      i++;
    }
    return results;
  }

  public static void main(String[] args) {
    int n = 10;
    System.out.println("magicRecursive(" + n + "): " + magicRecursive(n));
    System.out.println("magicFor(" + n + "): " + magicFor(n));
    System.out.println("magicWhile(" + n + "): " + magicWhile(n));
    System.out.println(
      "magicRecursive, magicFor and magicWhile have the same result"
    );
  }
}
