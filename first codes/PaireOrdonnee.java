public class PaireOrdonnee<T extends Comparable<T>> extends Paire<T> {
    public PaireOrdonnee(T first, T second) {
        super(first, second);
        // TODO Auto-generated constructor stub
    }

    public T getPetite() {
        if (first.compareTo(second) < 0) {
            return getFirst();
        } else {
            return getSecond();
        }
    }

    public T getGrande() {
        if (first.compareTo(second) > 0) {
            return getFirst();
        } else {
            return getSecond();
        }
    }

    public static void main(String[] args) {
        PaireOrdonnee<Integer> numbers = new PaireOrdonnee<Integer>(8, 5);
        System.out.println(numbers.getPetite());
        System.out.println(numbers.getGrande());

    }
}
