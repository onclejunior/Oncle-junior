public class Paire<T> {
    protected T first;
    protected T second;

    public Paire(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }

    public static void main(String[] args) {
        Paire<String> year = new Paire<String>("January", "First");
        System.out.print("The month of " + year.getFirst());
        System.out.println(" is the " + year.getSecond() + " month of the year.");
    }
}