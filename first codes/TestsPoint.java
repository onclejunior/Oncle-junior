import static org.junit.Assert.*;
import org.junit.Test;

public class TestsInterfacePoint {
    private InterfacePoint i = new ImplementInterfacePoint(abs, ord);

    @Test
    public void testGetOrd() {
        assertEquals("L'ordonnee est: ", ordonnee, i.getOrd());
    }

    @Test
    public void testGetAbs() {
        assertEquals("L'abcisse est: ", abcisse, i.getAbs());
    }

    @Test
    public void testSetOrd() {
        assertEquals("La nouvelle ordonnee est: ", ord, i.setOrd());
    }

    @Test
    public void testSetAbstract() {
        assertEquals("La nouvelle abcisse est: ", abs, i.setAbs());
    }

    @Test
    public void testAdd() {
        assertEquals("La somme des deux points est: " + (P1.getAbs() == p2.getAbs()) + ", "
                + ((p1.getOrd()) + p2.getOrd()) + ")");
    }

    @Test
    public void testEquals() {
        assertEquals(
                "les deux points sont egaux: (" + (P1.getAbs() == p2.getAbs()) && ((p1.getOrd()) == p2.getOrd()) + ")");
        assertEquals("Les deux points sont differents: (" + (P1.getAbs() != p2.getAbs())
                || ((p1.getOrd()) != p2.getOrd()) + ")");
    }
}