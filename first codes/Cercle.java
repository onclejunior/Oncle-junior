public class Cercle extends Point {
    public final double pi = 3.14;
    private final float rayon;

    public Cercle(float x, float y, float rayon) {
        super(x, y);
        this.rayon = rayon;
    }

    public float getRayon() {
        return rayon;
    }

    void Cercle(float rayon) {
        super.Point(x, y);
        System.out.println(
                " je suis le centre d'un cercle de rayon " + rayon + " cm et d'Aire " + rayon * rayon * pi + "cm*2");
    }

    public static void main(String[] args) {
        Cercle a = new Cercle(1, 3, 3);
        a.Cercle(3);
    }
}
