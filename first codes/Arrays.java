public class Arrays {
    // a)
    public static int getMin(int[] arr) {
        int k = 0;
        int tmp = arr[k];
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        return arr[k];
    }

    // b)
    public static String printArray(int[] arr) {
        String s = "";
        for (int i = 1; i < arr.length - 1; i++) {
            s += ", " + arr[i];
        }
        if (arr.length == 0) {
            return "[]";
        } else if (arr.length == 1) {
            return "[" + arr[0] + "]";
        } else {
            return "[" + arr[0] + s + ", " + arr[arr.length - 1] + "]";
        }
    }

    // c)
    public static int[] mul(int n) {
        int[] a = new int[n];
        int[] b = new int[n];
        int[] c = new int[n];
        for (int i = a.length - 1; i > 0; i--) {
            a[i] = i;
            int k = 0;
            int j = a.length - 1;
            while ((j > 0) && (k < a.length - 1)) {
                {
                    b[k] = j;
                }
                j--;
                k++;
            }
            for (int l = 0; l < c.length; l++) {
                c[i] = a[i] * b[i];
            }
        }
        return c;
    }

    public static void main(String[] args) {
        int[] arr = new int[] { 10, 2, 7, 4 };
        System.out.println("getMin(arr): " + getMin(arr));
        System.out.println("printArray(arr): " + printArray(arr));
        System.out.println("printArray(mul(100)): " + printArray(mul(100)));
    }
}
