import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;

public class Gestionnaire implements ActionListener {
    private static JTextField text;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Exemple ActionListener");
        text = new JTextField();
        text.setBounds(45, 50, 150, 20);
        JButton btn = new JButton("Formater le disque dur! ");
        btn.setBounds(70, 100, 100, 30);

        frame.add(btn);
        frame.add(text);
        frame.setSize(250, 250);
        frame.setLayout(null);
        frame.setVisible(true);

        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                text.setText("Formatage en cours!...");
            }
        });
    }
}
