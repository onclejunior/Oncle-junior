public class Rationnel {
    private int denominateur;

    public int getDenominateur(int denominateur) {
        return denominateur;
    }

    public void setDenominateur(int denominateur) {
        this.denominateur = denominateur;
    }

    private int numerateur;

    public int getNumerateur(int numerateur) {
        return numerateur;
    }

    public void setNumerateur(int numerateur) {
        this.numerateur = numerateur;
    }

    void ratio() {
        System.out.println("la valeur du nombre est le rationnel " + numerateur + "/" + denominateur);
    }

    public static void main(String[] args) {
        Rationnel r1 = new Rationnel();
        r1.numerateur = 1;
        r1.denominateur = 2;
        Rationnel r2 = new Rationnel();
        r2.numerateur = 4;
        r2.denominateur = 3;
        r1.ratio();
        r2.ratio();
    }
}