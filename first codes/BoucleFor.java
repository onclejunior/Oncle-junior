import java.util.ArrayList;

public class BoucleFor {
    public static void main(String[] args) {
        ArrayList<String> alpha = new ArrayList<String>();
        alpha.add("Rashford 10");
        alpha.add("Sancho 25");
        alpha.add("Martinez 6");
        alpha.add("DeGea 1");
        alpha.add("BFernandes 8");
        alpha.add("Ronaldo 7");
        alpha.add("Ericksen 14");
        alpha.add("Dalot 20");
        alpha.add("Malacia 12");
        alpha.add("McTominay 37");

        for (String player : alpha) {
            System.out.println(player);
        }
    }
}