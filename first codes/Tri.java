import java.util.ArrayList;

public class Tri {
    public static void main(String[] args) {
        ArrayList<Integer> alpha = new ArrayList<Integer>();
        alpha.add(10);
        alpha.add(25);
        alpha.add(6);
        alpha.add(1);
        alpha.add(8);
        alpha.add(7);
        alpha.add(14);
        alpha.add(20);
        alpha.add(12);
        alpha.add(37);

        /* Bubble Sort */
        boolean done = false;
        while (!done) {
            done = true;
            for (int i = 0; i < alpha.size() - 1; i++) {
                if (alpha.get(i) > alpha.get(i + 1)) {
                    int x = alpha.get(i);
                    alpha.set(i, alpha.get(i + 1));
                    alpha.set(i + 1, x);
                    done = false;

                }
            }
        }
        System.out.println(alpha);
    }
}