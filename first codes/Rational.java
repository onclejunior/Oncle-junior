spublic class Rational {
	public int num, den;

	public String toString() {
		return num + "/" + den;
	}

	static Rational create(int num, int den) {
		Rational r = new Rational();
		int p = ggT(num, den);
		r.num = num / p;
		r.den = den / p;
		return r;
	}

	public Rational copy() {
		return create(num, den);
	}

	public Rational opposite() {
		return create(den, num);
	}

	public Rational inverse() {
		return create(den, num);
	}

	private static int ggT(int a, int b) {
		if (b == 0)
			return a;
		return ggT(b, a % b);
	}

	public boolean isGreaterThanZero() {
		return num > 0 && den > 0 || num < 0 && den < 0;
	}

	public Rational plus(Rational other) {
		return create(num * other.den + den * other.num, other.den = den * other.den);
	}

	public Rational minus(Rational other) {
		return plus(other.opposite());
	}

	public Rational multiply(Rational other) {
		return create(num * other.num, den * other.den);
	}

	public Rational divide(Rational other) {
		return multiply(other.inverse());
	}

	public boolean equals(Rational other) {
		return num * other.den == den * other.num;
	}

	public int compareTo(Rational other) {
		if (equals(other))
			return 0;
		if (minus(other).isGreaterThanZero())
			return 1;
		else
			return -1;
	}

	public static void main(String[] args) {
		Rational a, b;
		a = new Rational();
		b = new Rational();
		a.num = 1;
		a.den = 2;
		b.num = 4;
		b.den = 3;
		System.out.println("a = 1/2 = " + a);
		System.out.println("b = 4/3 = " + b);
		System.out.println("compareTo(" + a + ", " + b + ") = "
				+ a.compareTo(b));
		System.out.println("1/2 = " + a.copy());
		System.out.println("-1/2 = " + a.opposite());
		System.out.println("11/6 = " + a.plus(b));
		System.out.println("2/3 = " + a.multiply(b));
		System.out.println("3/8 = " + a.divide(b));
	}
}
