public class Pile {
    /* Tableau contenant les elements de la pile. */
    private int[] tab;
    /*
     * Taille de la pile
     */
    private final int taille;
    /*
     * Indice du premier element non occupe dans le tableau .
     */
    private int firstFree;

    /*
     * Constructeur
     */
    Pile(int taille) {
        this.taille = 0;
    }

    /*
     * Constructeur de copie
     */

    public boolean estVide() {
        if (firstFree == 0)
            return true;
        else
            return false;
    }

    /*
     * Retourne vrai si et seulement si la pile est pleine .
     */
    public boolean estPleine() {
        if (firstFree + 1 == 0)
            return true;
        else
            return false;
    }

    /*
     * Retourne l ’ element se trouvant au sommet de la pile , −1 si la pile est
     * vide .
     */
    public int sommet() {
        if (!estVide) {
            return tab[tab.length - 1];
        } else {
            return -1;
        }
    }

    /*
     * Supprime l ’ element se trouvant au sommet de la pile , ne f a i t rien si la
     * pile est vide .
     */
    public void depile() {
        if (!estVide) {
            tab[tab.length - 1] = firstFree();
        }
    }

    /*
     * Ajoute data en haut de la pile , ne f a i t rien si la pile est pleine .
     */
    public void empile(int data) {
        if (!tab.estPleine) {
            tab[firstFree] = data;
        }
    }

    /*
     * Retourne une representation de la pile au format chaine de caracteres .
     */
    public String toString() {
        for (int element : tab)
            return toString(element);
    }

    /*
     * Teste le fonctionnement de la pile .
     */
    public static void main(String[] args) {
        Pile p = new Pile(30);
        int i = 0;
        while (!p.estPleine())
            p.empile(i++);
        System.out.println(p);
        while (!p.estVide()) {
            System.out.println(p.sommet());
            p.depile();
        }
    }
}