public class ImplementPoint implements Point {

    protected int abs;
    protected int ord;

    public ImplementPoint(int abs, int ord) {
        this.abs = abs;
        this.ord = ord;
    }

    public int getOrd() {
        return ord;
    }

    public int getAbs() {
        return abs;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public void setAbs(int abs) {
        this.abs = abs;
    }

    public InterfacePoint add(Point p1, Point p2) {
        Point p;
        p.setOrd(p1.getOrd() + p2.getOrd());
        p.setAbs(p1.getAbs() + p2.getAbs());
        return new Point(a, b);
    }

    public boolean equals(Point p1, Point p2) {
        if ((p1.getOrd() == p2.getOrd()) && (p1.getAbs() == p2.getAbs())) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Point p1 = new ImplementPoint(1, 3);
        Point p2 = new ImplementPoint(4, 5);
        System.out.println(p1.getAbs());
    }
}