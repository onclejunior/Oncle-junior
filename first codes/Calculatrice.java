import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;

public class Calculatrice implements ActionListener {

    JTextField operand = new JTextField();
    JLabel result = new JLabel();

    private KeyListener getKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                afficheCharacter();
            }
        };
    }

    private JPanel getMainPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 6));
        operand.addKeyListener(getKeyListener());
        panel.add(operand);
        panel.add(result);
        JButton equal = new JButton("=");
        JButton seven = new JButton("7");
        JButton eight = new JButton("8");
        JButton nine = new JButton("9");
        JButton div = new JButton("/");
        JButton mul = new JButton("*");
        JButton comma = new JButton(",");
        JButton four = new JButton("4");
        JButton five = new JButton("5");
        JButton six = new JButton("6");
        JButton add = new JButton("+");
        JButton min = new JButton("-");
        JButton zero = new JButton("0");
        JButton one = new JButton("1");
        JButton two = new JButton("2");
        JButton tree = new JButton("3");

        panel.add(equal);
        panel.add(seven);
        panel.add(eight);
        panel.add(nine);
        panel.add(div);
        panel.add(mul);
        panel.add(comma);
        panel.add(four);
        panel.add(five);
        panel.add(six);
        panel.add(add);
        panel.add(min);
        panel.add(zero);
        panel.add(one);
        panel.add(two);
        panel.add(tree);

        zero.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                operand.setText("0");
            }
        });

        return panel;
    }

    private void afficheCharacter() {
        try {
            int k = Integer.parseInt(operand.getText());
            result.setText("" + k);
        } catch (NumberFormatException e) {
            result.setText("");
        }
    }

    public Calculatrice() {
        JFrame frame = new JFrame();
        frame.setTitle(" Calculator ");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(getMainPanel());
        frame.setVisible(true);
        frame.pack();
    }

    public static void main(String[] args) {
        new Calculatrice();
    }
}
