import interactables.Interactable;
import rooms.Room;

public class Person {

    private String name;
    private Room currentRoom;
    private House currentHouse;
    Interactable[] interactables;

    public Person(String name, House currentHouse, Room currentRoom) {
        this.name = name;
        this.currentRoom = currentRoom;
        this.currentHouse = currentHouse;
        this.interactables = currentRoom.getInteractables();
    }

    // Move to the next room, if there is a room in the given direction
    public void move(String direction) {
        if (direction.equals("south") & currentRoom.getRow() < currentHouse.getHeight()) {
            System.out.println("\n" + name + " moved south");
            currentRoom = currentHouse.getRoom(currentRoom.getRow() + 1, currentRoom.getColumn());
            updateInteractables();
        } else if (direction.equals("east") & currentRoom.getColumn() + 1 < currentHouse.getWidth()) {
            System.out.println("\n" + name + " moved east");
            currentRoom = currentHouse.getRoom(currentRoom.getRow(), currentRoom.getColumn() + 1);
            updateInteractables();
        } else if (direction.equals("west") & currentRoom.getColumn() - 1 >= 0) {
            System.out.println("\n" + name + " moved west");
            currentRoom = currentHouse.getRoom(currentRoom.getRow(), currentRoom.getColumn() - 1);
            updateInteractables();
        } else if (direction.equals("north") & currentRoom.getRow() - 1 >= 0) {
            System.out.println("\n" + name + " moved north");
            currentRoom = currentHouse.getRoom(currentRoom.getRow() + 1, currentRoom.getColumn());
            updateInteractables();
        } else {
            System.out.println("There is no other room in the " + direction);
        }
    }

    // Save the current interactables in person
    public void updateInteractables() {
        interactables = currentRoom.getInteractables();
    }

    // List every interactable the person can see in the room
    public void lookAround() {
        System.out.println();
        Interactable[] interactables = currentRoom.getInteractables();
        System.out.println("This is a list of some things you can see in the " + currentRoom.getName() + ": ");
        if (interactables.length == 0)
            System.out.println("There is nothing.");
        else
            for (int i = 0; i < interactables.length; i++)
                System.out.println(interactables[i].getName());
    }

    // Give back informations about the room and house the person is in
    public void whereAmI() {
        System.out.println(
                "\n" + name + " is in room: " + currentRoom.getName() + " in the house: " + currentHouse.getName());
    }

    // Give out the text abut what happens when an interactable is used
    public void use(String obj) {
        boolean found = false;
        for (int i = 0; i < interactables.length; i++) {
            if (interactables[i].getName().equals(obj)) {
                interactables[i].use();
                found = true;
            }
        }
        if (!found)
            System.out.println("\nNothing found");

    }
}
