import rooms.Room;

public class House {
    private String name;
    private Room[][] rooms;
    private int width;
    private int height;

    public House(Room[][] rooms, String name) {
        this.rooms = rooms;
        width = rooms[0].length;
        height = rooms.length;
        this.name = name;
        setRoomPositions();
    }

    // Set the position of every room to their position in the array
    private void setRoomPositions() {
        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < width; j++) {
                rooms[i][j].setPos(i, j);
            }
        }
    }

    // Getters
    public Room getRoom(int num1, int num2) {
        return rooms[num1][num2];
    }

    public String getName() {
        return name;
    }

    public Room[][] getRooms() {
        return rooms;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
    // End getter methods
}
