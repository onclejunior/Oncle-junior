import interactables.Bed;
import interactables.Interactable;
import interactables.Shower;
import interactables.Toilet;
import rooms.Bathroom;
import rooms.Bedroom;
import rooms.Foyer;
import rooms.Hallway;
import rooms.Kitchen;
import rooms.Livingroom;
import rooms.Room;
import rooms.WinterGarden;

public class HouseDemo {
    House myHouse;
    Person user;

    public HouseDemo() {
        // Create interactables
        Bed bed = new Bed();
        Shower shower = new Shower();
        Toilet toilete = new Toilet();

        // Create the rooms
        Foyer foyer = new Foyer("foyer", new Interactable[] {});
        Bedroom bedroom = new Bedroom("bedroom", new Interactable[] { bed });
        Bathroom bathroom1 = new Bathroom("guest bathroom", new Interactable[] { shower, toilete });
        Bathroom bathroom2 = new Bathroom("normal bathroom", new Interactable[] {});
        Kitchen kitchen = new Kitchen("kitchen", new Interactable[] {});
        Livingroom livingroom = new Livingroom("livingroom", new Interactable[] {});
        Hallway hallway1 = new Hallway("hallway 1", new Interactable[] {});
        Hallway hallway2 = new Hallway("hallway 2", new Interactable[] {});
        WinterGarden winterGarden = new WinterGarden("winter garden", new Interactable[] {});

        // Create the house
        this.myHouse = new House(new Room[][] { { foyer, bedroom, bathroom1 },
                { bathroom2, kitchen, livingroom },
                { hallway1, hallway2, winterGarden } }, "Villa");

        // Create the person in the house
        this.user = new Person("Hans", myHouse, myHouse.getRooms()[0][0]);
    }

    public void run() {
        // Starting in the foyer
        user.whereAmI();
        // Moving in the first hallway
        user.move("east");
        user.whereAmI();
        // Failing to go north, because there is no room
        user.move("north");
        user.whereAmI();
        // Going in the bathroom
        user.move("east");
        user.whereAmI();
        // There should be a shower an da toilette
        user.lookAround();
        user.use("Shower");
        user.use("Toilet");
        // Moving in the livingroom
        user.move("south");
        user.whereAmI();
        user.lookAround();
    }
}
