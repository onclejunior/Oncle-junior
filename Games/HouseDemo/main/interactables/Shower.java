package interactables;

public class Shower implements Interactable {
    @Override
    public void use() {
        System.out.println("The shower was used.");
    }

    @Override
    public String getName() {
        return "Shower";
    }
}
