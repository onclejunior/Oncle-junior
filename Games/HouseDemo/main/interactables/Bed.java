package interactables;

public class Bed implements Interactable {
    @Override
    public void use() {
        System.out.println("You slept and feel less tired.");
    }

    @Override
    public String getName() {
        return "Bed";
    }
}
