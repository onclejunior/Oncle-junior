package interactables;

public interface Interactable {
    // print in the terminal what happens, when the interactable is used
    public void use();

    // Just return the name as a string
    public String getName();
}
