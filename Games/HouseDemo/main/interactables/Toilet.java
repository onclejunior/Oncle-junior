package interactables;

public class Toilet implements Interactable {
    @Override
    public void use() {
        System.out.println("The toilet was used.");
    }

    @Override
    public String getName() {
        return "Toilet";
    }
}
