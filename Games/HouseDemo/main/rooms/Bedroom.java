package rooms;

import interactables.Interactable;

public class Bedroom extends Room {
    public Bedroom(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
