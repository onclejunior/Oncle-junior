package rooms;

import interactables.Interactable;

public class Hallway extends Room {
    public Hallway(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
