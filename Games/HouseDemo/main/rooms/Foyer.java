package rooms;

import interactables.Interactable;

public class Foyer extends Room {
    public Foyer(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
