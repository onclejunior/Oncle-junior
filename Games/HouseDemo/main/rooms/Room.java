package rooms;

import interactables.Interactable;

public class Room {
    // attributes
    private String roomName;
    private Interactable[] interactables;
    private int row;
    private int column;

    // constructor
    public Room(String roomName, Interactable[] interactables) {
        this.roomName = roomName;
        this.interactables = interactables;
    }

    // methods
    public void setPos(int i, int j) {
        row = i;
        column = j;
    }

    public String getName() {
        return roomName;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Interactable[] getInteractables() {
        return interactables;
    }
}
