package rooms;

import interactables.Interactable;

public class Kitchen extends Room {
    public Kitchen(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
