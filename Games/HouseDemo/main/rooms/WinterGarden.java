package rooms;

import interactables.Interactable;

public class WinterGarden extends Room {
    public WinterGarden(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
