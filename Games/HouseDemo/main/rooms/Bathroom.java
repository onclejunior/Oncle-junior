package rooms;

import interactables.Interactable;

public class Bathroom extends Room {
    public Bathroom(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
