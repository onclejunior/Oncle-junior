package rooms;

import interactables.Interactable;

public class Livingroom extends Room {
    public Livingroom(String roomName, Interactable[] interactables) {
        super(roomName, interactables);
    }
}
