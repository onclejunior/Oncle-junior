class Kunde {
    name: string;
    constructor(name: string){
    this.name= name;
    }
    getName(){
        return this.name;
    }
}
    
class Bank {
    name: string;
    blz: number;
    kunden: Kunde[];
    constructor(name: string, blz: number){
        this.name = name;
        this.blz = blz;
        this.kunden = [];
    }
    
    getName(){
        return this.name;
    }
    
    getBlz(){
        return this.blz
    }
    
    getKunden(){
        return this.kunden;
    }

    addKunde(kunde: Kunde){
        this.kunden.push(kunde);
    }
}
    var alice = new Kunde("Alice");
    var bob = new Kunde("Bob");
    var charlie = new Kunde("Charlie");
    var sparkasse = new Bank("Sparkasse", 43000);
    sparkasse.addKunde(alice);
    sparkasse.addKunde(bob);
    sparkasse.addKunde(charlie);
    var sparkassenKunden = sparkasse.getKunden();
    for(let kunde in sparkassenKunden){
        console.log(sparkassenKunden[kunde].getName());
    }