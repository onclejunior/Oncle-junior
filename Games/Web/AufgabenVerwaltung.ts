import express, { Request, Response } from 'express';

const app = express();
app.use(express.json());

interface Task {
    id: number;
    title: string;
    completed: boolean;
}

let tasks: Task[] = [];

// Route: Alle Aufgaben abrufen
app.get('/tasks', (req: Request, res: Response) => {
    res.json(tasks);
});

// Route: Neue Aufgabe erstellen
app.post('/tasks', (req: Request, res: Response) => {
    const { title } = req.body;
    const newTask: Task = {
        id: tasks.length + 1,
        title,
        completed: false,
    };
    tasks.push(newTask);
    res.status(201).json(newTask);
});

// Route: Aufgabe abschließen
app.patch('/tasks/:id', (req: Request, res: Response) => {
    const id = parseInt(req.params.id);
    const task = tasks.find((task) => task.id === id);

    if (task) {
    task.completed = true;
    res.json(task);
    } else {
        res.status(404).json({ message: 'Task not found' });
    }
});

// Server starten
app.listen(3000, () => {
    console.log('Task Manager API running on http://localhost:3000');
});
