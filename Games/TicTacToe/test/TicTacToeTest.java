import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


public class TicTacToeTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private ByteArrayInputStream inContent;
    private final InputStream originalIn = System.in;

    private final String player_x_win_msg = "The winner is: X";
    private final String player_o_win_msg = "The winner is: O";
    private final String draw_msg = "draw";

    @Before
    public void setUpStreams() {
        // Redirecting Output Stream
        System.setOut(new PrintStream(outContent));
        // Note: Input Stream will be set up in each test method as needed
    }

    @After
    public void restoreStreams() {
        // Restoring the original streams
        System.setOut(originalOut);
        System.setIn(originalIn);
    }

    @Test
    public void testPlayerXWin() {
        String input = "1\n2\n4\n5\n7\n";
        inContent = new ByteArrayInputStream(input.getBytes());
        System.setIn(inContent);

        Main.main(new String[] {});

        // Checking for Player X win message
        String output = outContent.toString().toUpperCase();
        assertTrue(output.contains(player_x_win_msg.toUpperCase()));
        assertFalse(output.contains(player_o_win_msg.toUpperCase()));
        assertFalse(output.contains(draw_msg.toUpperCase()));
    }

    @Test
    public void testPlayerOWin() {
        String input = "1\n2\n4\n5\n6\n8\n";
        inContent = new ByteArrayInputStream(input.getBytes());
        System.setIn(inContent);

        Main.main(new String[] {});

        // Checking for Player O win message
        String output = outContent.toString().toUpperCase();
        assertFalse(output.contains(player_x_win_msg.toUpperCase()));
        assertTrue(output.contains(player_o_win_msg.toUpperCase()));
        assertFalse(output.contains(draw_msg.toUpperCase()));
    }

    @Test
    public void testDraw() {
        String input = "1\n2\n3\n4\n5\n7\n6\n9\n8\n";
        inContent = new ByteArrayInputStream(input.getBytes());
        System.setIn(inContent);

        Main.main(new String[] {});

        // Checking for draw message
        String output = outContent.toString().toUpperCase();
        assertFalse(output.contains(player_x_win_msg.toUpperCase()));
        assertFalse(output.contains(player_o_win_msg.toUpperCase()));
        assertTrue(output.contains(draw_msg.toUpperCase()));
    }
}
