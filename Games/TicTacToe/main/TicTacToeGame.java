import java.util.Scanner;

public class TicTacToeGame {
    Board board;

    public TicTacToeGame() {
        this.board = new Board();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println(board.toString());
            System.out.println();
            System.out.print("Current turn: ");

            if (board.player == 0)
                System.out.println("X");
            else
                System.out.println("O");

            System.out.print("Where do you want to place next? ");
            int field = scanner.nextInt() - 1;

            // Check that the field is on the board
            if (field < 0 || field > 8) {
                System.out.println("Invalid guess!");
                continue;
            }

            // Place the mark for the current player
            // Returns false if the placement didn't work for any reason
            if (!this.board.place(field)) {
                System.out.println("That spot is already taken!");
                continue;
            }

            // Check if a player has won the game
            int winner = this.board.getWinner();
            if (winner != -1) {
                System.out.print("The winner is: ");
                if (winner == 0)
                    System.out.println("X");
                else
                    System.out.println("O");

                break;
            }

            // Check if it is a draw
            if (board.isDraw()) {
                System.out.println("No more turns left. This game ends in a draw!");
                break;
            }
        }

        System.out.println(board.toString());
        scanner.close();
    }
}