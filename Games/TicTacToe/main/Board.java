import java.util.Arrays;

public class Board {
    int player = 0;
    int[] state; // Here we're using -1 for empty, 0 for X and 1 for O

    public Board() {
        this.state = new int[9];
        // Mark all cells as empty
        Arrays.fill(this.state, -1);
    }

    public boolean place(int index) {
        if (state[index] != -1) {
            System.out.println("Field is already full.");
            return false;
        }

        state[index] = player;

        player++;
        player %= 2;

        return true;
    }

    public int getWinner() {
        // List of all indices for rows, columns and diagonals
        int[][] rows = new int[][] {
                { 0, 1, 2 },
                { 3, 4, 5 },
                { 6, 7, 8 },
                { 0, 3, 6 },
                { 1, 4, 7 },
                { 2, 5, 8 },
                { 0, 4, 8 },
                { 2, 4, 6 }
        };

        for (int[] row : rows) {
            // This checks that the entire row has the same value
            boolean rowMatches = Arrays.stream(row).map(i -> state[i]).distinct().count() == 1;

            // This checks that the row isn't completely empty
            boolean rowEmpty = state[row[0]] == -1;

            if (rowMatches && !rowEmpty)
                return state[row[0]];
        }

        return -1;
    }

    public boolean isDraw() {
        for (int x : state) {
            if (x == -1) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < 9; i++) {
            if (state[i] == 0)
                result += "X ";
            else if (state[i] == 1)
                result += "O ";
            else
                result += "_ ";

            if ((i + 1) % 3 == 0)
                result += "\n";
        }
        return result;
    }
}
