package org.example;

import java.util.Arrays;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MyApplication extends Application {

  private char[] word;
  private char[] guessedLetters;
  private int wrongGuesses = 0;
  private boolean gameOver = false;
  private int maxGuesses = 6;


  private Label guessedLettersLabel;
  private Label wrongGuessesLabel;
  private TextField inputField;
  private Button guessButton;

  @Override
  public void start(Stage primaryStage) {
    // Interface components
    VBox root = new VBox(10);
    root.setPadding(new Insets(10));

    Label titleLabel = new Label("Hangman Game");
    titleLabel.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");

    guessedLettersLabel = new Label();
    wrongGuessesLabel = new Label();

    inputField = new TextField();
    guessButton = new Button("Guess");
    guessButton.setOnAction(event -> makeGuess());

    root
      .getChildren()
      .addAll(
        titleLabel,
        guessedLettersLabel,
        wrongGuessesLabel,
        inputField,
        guessButton
      );

    Scene scene = new Scene(root, 300, 200);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Hangman Game");
    primaryStage.show();

    // Start the game
    startGame();
  }

  private void startGame() {
    // Read the secret word (for simplicity, you can replace this with a random word generator)
    word = "hello".toCharArray();
    guessedLetters = new char[word.length];
    Arrays.fill(guessedLetters, '_');

    updateLabels();
  }

  private void makeGuess() {
    if (!gameOver) {
      String input = inputField.getText();
      if (input.length() == 1) {
        char letter = input.charAt(0);
        boolean correctlyGuessed = false;

        for (int j = 0; j < word.length; j++) {
          if (word[j] == letter) {
            guessedLetters[j] = letter;
            correctlyGuessed = true;
          }
        }

        if (!correctlyGuessed) {
          wrongGuesses++;
        }

        updateLabels();

        if (Arrays.equals(word, guessedLetters)) {
          gameOver = true;
          guessedLettersLabel.setText(
            "Congratulations! You guessed the word: " + Arrays.toString(word)
          );
        } else if (wrongGuesses >= maxGuesses) {
          gameOver = true;
          guessedLettersLabel.setText(
            "Game Over! The word was: " + Arrays.toString(word)
          );
        }
      } else {
        guessedLettersLabel.setText("Please enter a single character.");
      }
    } else {
      guessedLettersLabel.setText("The game is over.");
    }

    inputField.clear();
  }

  private void updateLabels() {
    guessedLettersLabel.setText(
      "Guessed letters: " + Arrays.toString(guessedLetters)
    );
    wrongGuessesLabel.setText("Wrong guesses: " + wrongGuesses);
  }
}
