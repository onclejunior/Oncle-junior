package org.example;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

public class MyCanvas extends Canvas{
    private GraphicsContext graphics;

    public MyCanvas(){
        setWidth(100);
        setHeight(300);
        graphics = getGraphicsContext2D();
        addEventHandler(MouseEvent.MOUSE_CLICKED, event -> paintRectangle(event));
    }
    private void paintRectangle(MouseEvent e){
        graphics.strokeRect(e.getX(), e.getY(), 25, 25);
    }
}