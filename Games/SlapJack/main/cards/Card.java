package cards;

public abstract class Card {

    protected char symbol;

    public Card(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "" + symbol;
    }
}
