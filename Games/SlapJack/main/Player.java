import cards.Card;

import java.util.ArrayList;

public class Player {

    public ArrayList<Card> personalStack;

    /**
     * the key this player has to press to SLAP a card on the board
     * */
    public char keyboardKey;

    public Player(char keyboardKey){
        this.keyboardKey = keyboardKey;
    }
}
