import cards.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class SlapJackGame {

    protected final int startingNumberCards = 10;

    protected Board board;
    protected Player playerOne;
    protected Player playerTwo;
    protected boolean playerOneTurn;
    protected Card[] possibleCards;
    protected Random rng;

    public SlapJackGame() {
        this.board = new Board();
        this.playerOne = new Player('a');
        this.playerTwo = new Player('l');
        this.possibleCards = new Card[] { new Ace(), new Jack(), new King(), new Queen() };
        this.rng = new Random();
    }

    private boolean isGameOver() {
        return playerOne.personalStack.isEmpty() || playerTwo.personalStack.isEmpty();
    }

    /**
     * A random card is played from the deck of the current player
     */
    private void playAndLogNextCard() {
        // Pick the player to place the next card on the board
        Player currentPlayer;
        if (playerOneTurn) {
            currentPlayer = playerOne;
        } else {
            currentPlayer = playerTwo;
        }
        playerOneTurn = !playerOneTurn;

        // Remove card from the player's deck and place it on the board
        int randomCard = rng.nextInt(currentPlayer.personalStack.size());
        Card playedCard = currentPlayer.personalStack.remove(randomCard);
        board.cardStack.add(playedCard);

        // Print out the card
        System.out.println("Card played: " + playedCard.toString());
    }

    /**
     * Players start with same deck. board starts empty.
     */
    private void initGame() {
        ArrayList<Card> playerStartingDeck = new ArrayList<>();
        for (int i = 0; i < startingNumberCards; i++) {
            playerStartingDeck.add(possibleCards[i % possibleCards.length]);
        }

        playerOne.personalStack = new ArrayList<>(playerStartingDeck);
        playerTwo.personalStack = new ArrayList<>(playerStartingDeck);
        board.cardStack = new ArrayList<>();

        playerOneTurn = true;
    }

    private void logCardCount() {
        System.out.println("Board: " + board.cardStack.size() + " cards");
        System.out.println("Player One: " + playerOne.personalStack.size() + " cards");
        System.out.println("Player Two: " + playerTwo.personalStack.size() + " cards \n");
    }

    private void logWinner(Player winner) {
        String playerName;

        if (winner == playerOne) {
            playerName = "Player One";
        } else {
            playerName = "Player Two";
        }
        System.out.println(playerName + " WON the game!");
    }

    public void play() {
        initGame();

        Scanner scanner = new Scanner(System.in);
        while (!isGameOver()) {
            logCardCount();
            playAndLogNextCard();

            boolean isJack = board.cardStack.get(board.cardStack.size() - 1) instanceof Jack;
            char consoleInput = scanner.next().charAt(0);
            while (true) {
                // Pick the player that slapped the Jack
                Player playerWhoSlapped;
                if (consoleInput == playerOne.keyboardKey) {
                    playerWhoSlapped = playerOne;
                } else if (consoleInput == playerTwo.keyboardKey) {
                    playerWhoSlapped = playerTwo;
                } else {
                    // No one slapped the Jack
                    break;
                }

                // Distribute the cards with respect to whether there is a Jack on the board
                if (isJack) {
                    playerWhoSlapped.personalStack.addAll(board.cardStack);
                    board.cardStack.clear();
                    break;
                } else {
                    playerWhoSlapped.personalStack.clear();
                    break;
                }
            }
        }

        // Log the winner
        if (playerOne.personalStack.isEmpty()) {
            logWinner(playerTwo);
        } else if (playerTwo.personalStack.isEmpty()) {
            logWinner(playerOne);
        }

        scanner.close();
    }
}
