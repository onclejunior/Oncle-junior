import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.assertTrue;

import java.io.PrintStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class SlapJackTest {
    private final String WIN_KEYWORD = "WON";
    private final String WIN_PLAYER1_KEYWORD = "Player One WON the game!";
    private final String WIN_PLAYER2_KEYWORD = "Player Two WON the game!";
    private final String JACK_KEYWORD = "J";
    private final String CARD_PLAYED_KEYWORD = "Card played:";
    private final String SLAP_PLAYER1_KEYWORD = "a";
    private final String SLAP_PLAYER2_KEYWORD = "l";
    private final String NEXT_KEYWORD = "n";

    private final int DELAY = 5;
    private final int MAX_WAIT_TIME = 1000;
    private final int MAX_NUM_ATTEMPTS = MAX_WAIT_TIME / DELAY;

    private final ByteArrayOutputStream capturedOutput = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private PipedInputStream inPipe;
    private PipedOutputStream outPipe;
    private PrintStream gameInput;
    private Thread gameThread;

    @Before
    public void setUp() throws IOException {
        // Redirecting System.out to capture output
        System.setOut(new PrintStream(capturedOutput));

        // Setting up piped streams for input injection
        inPipe = new PipedInputStream();
        outPipe = new PipedOutputStream(inPipe);
        gameInput = new PrintStream(outPipe);
        System.setIn(inPipe);

        // Initialize a new game thread
        gameThread = new Thread(() -> {
            Main.main(new String[] {});
        });
    }

    @After
    public void restore() {
        // Restore original System.out and System.in
        System.setOut(originalOut);
        System.setIn(System.in);

        // Close streams
        try {
            gameInput.close();
            outPipe.close();
            inPipe.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean roughlyEquals(String s1, String s2) {
        return s1.trim().toUpperCase().equals(s2.trim().toUpperCase());
    }

    private String testSlapJackGame(String action1, String action2) throws InterruptedException {
        // Start a thread with the game
        gameThread.start();

        String gameResult;
        while (true) {
            // Read output from the game
            String gameOutput = "";
            for (int i = 0; i < MAX_NUM_ATTEMPTS; i++) {
                if (gameOutput.contains(WIN_KEYWORD) || gameOutput.contains(CARD_PLAYED_KEYWORD)) {
                    break;
                }
                Thread.sleep(DELAY); // Sleep to sync with game thread
                gameOutput = capturedOutput.toString();
            }

            // Clear the captured output for next read
            capturedOutput.reset();

            // Break if game is over
            if (gameOutput.contains(WIN_KEYWORD)) {
                gameResult = gameOutput;
                break;
            }

            // Check if the next card has been printed out
            String[] cardPlayedSplit = gameOutput.split(CARD_PLAYED_KEYWORD);
            assertTrue("Cannot find the message \"Card played: X\" in the standard output.",
                    cardPlayedSplit.length >= 2);

            // Pick the last occurrence of "Card played:"
            String cardPlayed = cardPlayedSplit[cardPlayedSplit.length - 1];
            if (cardPlayed.length() > 2) {
                cardPlayed = cardPlayed.substring(0, 2);
            }

            // Do action1 whenever the Jack comes up, and do action 2 otherwise
            if (roughlyEquals(cardPlayed, JACK_KEYWORD)) {
                gameInput.println(action1);
            } else {
                gameInput.println(action2);
            }

            // Flush to ensure it gets processed
            gameInput.flush();
        }

        // Close the game thread
        gameThread.interrupt();

        return gameResult;
    }

    @Test
    public void testPlayerOneWin() throws InterruptedException {
        // If the Jack comes up, let player 1 slap the pile.
        // If there is no Jack, skip to the next player.
        String gameResult = testSlapJackGame(SLAP_PLAYER1_KEYWORD, NEXT_KEYWORD);
        assertTrue("Expected player 1 to win by slapping the Jack.", roughlyEquals(gameResult, WIN_PLAYER1_KEYWORD));
    }

    @Test
    public void testPlayerTwoWin() throws Exception {
        // If the Jack comes up, let player 2 slap the pile.
        // If there is no Jack, skip to the next player.
        String gameResult = testSlapJackGame(SLAP_PLAYER2_KEYWORD, NEXT_KEYWORD);
        assertTrue("Expected player 2 to win by slapping the Jack.", roughlyEquals(gameResult, WIN_PLAYER2_KEYWORD));
    }

    @Test
    public void testPlayerOneLose() throws Exception {
        // If the Jack comes up, skip to the next player.
        // If there is no Jack, let player 1 slap the pile (and lose)
        String gameResult = testSlapJackGame(NEXT_KEYWORD, SLAP_PLAYER1_KEYWORD);
        assertTrue("Expected player 1 to lose by slapping the wrong card.",
                roughlyEquals(gameResult, WIN_PLAYER2_KEYWORD));
    }

    @Test
    public void testPlayerTwoLose() throws Exception {
        // If the Jack comes up, skip to the next player.
        // If there is no Jack, let player 2 slap the pile (and lose)
        String gameResult = testSlapJackGame(NEXT_KEYWORD, SLAP_PLAYER2_KEYWORD);
        assertTrue("Expected player 2 to lose by slapping the wrong card.",
                roughlyEquals(gameResult, WIN_PLAYER1_KEYWORD));
    }
}