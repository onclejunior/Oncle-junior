import java.util.Scanner;

public class HangmanGame {
    public static void main(String[] args) {

        // Scanner to read the input
        Scanner scanner = new Scanner(System.in);

        // Read the secret word and turn it into an array of characters
        System.out.println("Enter the secret word:");
        char[] word = scanner.nextLine().toCharArray();

        // Array to track guessed letters
        char[] guessedState = new char[word.length];
        for (int i = 0; i < guessedState.length; i++) {
            guessedState[i] = '_';
        }

        // Number of wrong guesses allowed
        int wrongGuessesAllowed = 6;
        int wrongGuesses = 0;

        // To track if the game is won
        int correctGuessesNeeded = word.length;

        // Game loop
        int correctGuesses = 0;
        while (true) {
            System.out.println("\nCurrent progress: ");
            for (char c : guessedState) {
                System.out.print(c + " ");
            }
            System.out.println("\nYou have " + (wrongGuessesAllowed - wrongGuesses) + " wrong guesses left.");

            // Ask for the next guess
            System.out.print("Guess a letter: ");
            String input = scanner.nextLine().toLowerCase();
            if (input.length() != 1 || !Character.isLetter(input.charAt(0))) {
                System.out.println("Invalid input. Please, guess a single letter.");
                continue;
            }
            char guessedLetter = input.charAt(0);

            // Check and apply the guess
            boolean letterFound = false;
            for (int i = 0; i < word.length; i++) {
                if (word[i] == guessedLetter && guessedState[i] == '_') {
                    guessedState[i] = guessedLetter;
                    letterFound = true;
                    correctGuesses++;
                }
            }

            // Print a message based on whether the letter was found
            if (!letterFound) {
                wrongGuesses++;
                System.out.println("Wrong guess!");
                if (wrongGuesses == wrongGuessesAllowed) {
                    System.out.println("Game Over! The word was: " + new String(word));
                    break;
                }
            } else {
                if (correctGuesses == correctGuessesNeeded) {
                    System.out.println("Congratulations! You've guessed the word: " + new String(word));
                    break;
                }
            }
        }

        scanner.close();
    }
}
