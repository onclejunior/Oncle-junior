package fishables;

public class Herring extends Fishable {
    public Herring() {
        super("Herring", 5, 22, 4);
    }
}
