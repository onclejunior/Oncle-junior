package fishables;

public class Salmon extends Fishable {
    public Salmon() {
        super("Salmon", 4, 26, 7);
    }
}
