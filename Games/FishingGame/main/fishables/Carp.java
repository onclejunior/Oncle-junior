package fishables;

public class Carp extends Fishable {
    public Carp() {
        super("Carp", 2, 15, 3);
    }
}
