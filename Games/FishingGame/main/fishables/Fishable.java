package fishables;

import java.util.concurrent.ThreadLocalRandom;

public abstract class Fishable {
    protected String name;
    protected int weight;
    protected int value;
    protected boolean fished = false;

    public Fishable(String name, int minWeight, int maxWeight, int value) {
        this.name = name;
        this.weight = ThreadLocalRandom.current().nextInt(minWeight, maxWeight);
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getValue() {
        return value;
    }

    public boolean isFished() {
        return this.fished;
    }

    public boolean fish() {
        if (this.fished)
            return false;
        return this.fished = true;
    }
}
