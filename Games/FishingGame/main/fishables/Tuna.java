package fishables;

public class Tuna extends Fishable {
    public Tuna() {
        super("Tuna", 6, 100, 1);
    }
}
