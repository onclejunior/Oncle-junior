package fishables;

public class Shoe extends Fishable {
    public Shoe() {
        super("Shoe", 1, 5, -5);
    }
}
