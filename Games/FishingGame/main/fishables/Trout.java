package fishables;

public class Trout extends Fishable {
    public Trout() {
        super("Trout", 1, 8, 4);
    }
}
