package fishables;

public class Shark extends Fishable {
    public Shark() {
        super("Shark", 100, 500, 5);
    }
}
