package fishables;

public class ElectricScooter extends Fishable {
    public ElectricScooter() {
        super("Electric Scooter", 18, 25, -10);
    }
}
