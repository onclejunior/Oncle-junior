import fishables.*;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class FishingGame {
    private final int MAX_TOTAL_WEIGHT = 1000;

    private int width, height;
    private Fishable[][] board;
    private int totalScore;
    private int totalWeight;

    public FishingGame(int width, int height) {
        this.board = new Fishable[width][height];
        this.totalScore = 0;
        this.totalWeight = 0;

        this.width = width;
        this.height = height;

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                this.board[i][j] = getRandomFish();
            }
        }
    }

    public Fishable getRandomFish() {
        int select = ThreadLocalRandom.current().nextInt(100);
        if (select < 20)
            return new Carp();
        if (select < 28)
            return new Tuna();
        if (select < 40)
            return new Shoe();
        if (select < 49)
            return new ElectricScooter();
        if (select < 65)
            return new Trout();
        if (select < 70)
            return new Shark();
        if (select < 80)
            return new Sardine();
        if (select < 90)
            return new Herring();

        return new Salmon();
    }

    public void printBoard() {
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                System.out.print(this.board[i][j].isFished() ? "░" : "▓");
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("--------- Fishing Game ---------");
        System.out.println("Welcome to the pond. Good luck!");

        while (true) {
            // Print board and read input
            this.printBoard();
            System.out.print("Select an X-Coordinate: ");
            int input_i = scanner.nextInt() - 1;
            System.out.print("Select a Y-Coordinate: ");
            int input_j = scanner.nextInt() - 1;

            // Check if out of bounds
            if (input_i >= this.width || input_i < 0 || input_j >= this.height || input_j < 0) {
                System.out.println("These coordinates are not in the pond!");
                continue;
            }

            // Get a fish from the pond
            Fishable fish = this.board[input_i][input_j];
            if (fish.fish()) {
                System.out.println("You fished a " + fish.getName());
                System.out.println("Weight: " + fish.getWeight());
                System.out.println("Value: " + fish.getValue());
                this.totalScore += fish.getValue();
                this.totalWeight += fish.getWeight();
                System.out.println("Your current score is: " + this.totalScore);
                System.out.println("The total weight is: " + this.totalWeight);
            } else {
                System.out.println("You already fished here");
            }

            // Exit if too much weight
            if (totalWeight >= MAX_TOTAL_WEIGHT) {
                break;
            }

            // Exit if no more fish
            boolean fish_exhausted = true;
            for (int i = 0; i < this.width; i++) {
                for (int j = 0; j < this.height; j++) {
                    if (!this.board[i][j].isFished()) {
                        fish_exhausted = false;
                    }
                }
                if (!fish_exhausted) {
                    break;
                }
            }
            if (fish_exhausted) {
                break;
            }
        }

        System.out.println("Game over! Your score is: " + this.totalScore);
        scanner.close();
    }
}
