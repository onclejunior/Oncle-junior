import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.assertTrue;

import java.io.PrintStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FishingGameTest {
    private final String OVER_KEYWORD = "GAME OVER";
    private final String SCORE_KEYWORD = "YOUR SCORE IS:";
    private final String WEIGHT_KEYWORD = "WEIGHT:";
    private final String VALUE_KEYWORD = "VALUE:";
    private final int MAX_TOTAL_WEIGHT = 1000;

    private final int DELAY = 5;
    private final int MAX_WAIT_TIME = 1000;
    private final int MAX_NUM_ATTEMPTS = MAX_WAIT_TIME / DELAY;

    private final ByteArrayOutputStream capturedOutput = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private PipedInputStream inPipe;
    private PipedOutputStream outPipe;
    private PrintStream gameInput;
    private Thread gameThread;

    @Before
    public void setUp() throws IOException {
        // Redirecting System.out to capture output
        System.setOut(new PrintStream(capturedOutput));

        // Setting up piped streams for input injection
        inPipe = new PipedInputStream();
        outPipe = new PipedOutputStream(inPipe);
        gameInput = new PrintStream(outPipe);
        System.setIn(inPipe);

        // Initialize a new game thread
        gameThread = new Thread(() -> {
            Main.main(new String[] {});
        });
    }

    @After
    public void restore() {
        // Restore original System.out and System.in
        System.setOut(originalOut);
        System.setIn(System.in);

        // Close streams
        try {
            gameInput.close();
            outPipe.close();
            inPipe.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void testFishingGame(String[] actions) throws InterruptedException {
        // Regex patterns
        Pattern valuePattern = Pattern.compile("Value:\\s*(-?\\d+)", Pattern.CASE_INSENSITIVE);
        Pattern weightPattern = Pattern.compile("Weight:\\s*(-?\\d+)", Pattern.CASE_INSENSITIVE);
        Pattern scorePattern = Pattern.compile("Your score is:\\s*(-?\\d+)", Pattern.CASE_INSENSITIVE);

        String gameOutput = "";
        int totalWeight = 0;
        int totalScore = 0;
        for (int i = 0; i < actions.length; i++) {
            // Input the action
            gameInput.println(actions[i]);

            // Flush to ensure it gets processed
            gameInput.flush();

            // Read output for value and weight
            boolean isValue = false, isWeight = false;
            for (int j = 0; j < MAX_NUM_ATTEMPTS; j++) {
                Thread.sleep(DELAY); // Sleep to sync with game thread
                gameOutput = capturedOutput.toString().toUpperCase();
                isValue = gameOutput.contains(VALUE_KEYWORD);
                isWeight = gameOutput.contains(WEIGHT_KEYWORD);
                if (isValue && isWeight) {
                    break;
                }
            }

            assertTrue("Cannot find the message \"Value: X\" in the standard output.", isValue);
            assertTrue("Cannot find the message \"Weight: X\" in the standard output.", isWeight);

            Matcher valueMatcher = valuePattern.matcher(gameOutput);
            assertTrue("Message \"Value: X\" was found but it contains an invalid value.", valueMatcher.find());
            int value = Integer.parseInt(valueMatcher.group(1));
            totalScore += value;

            Matcher weightMatcher = weightPattern.matcher(gameOutput);
            assertTrue("Message \"Weight: X\" was found but it contains an invalid value.", weightMatcher.find());
            int weight = Integer.parseInt(weightMatcher.group(1));
            totalWeight += weight;

            // Break if exceeded max total weight
            if (totalWeight > MAX_TOTAL_WEIGHT) {
                break;
            }

            // Clear the captured output for next read (except for the last iteration)
            if (i != actions.length - 1) {
                capturedOutput.reset();
            }
        }

        // Read output for game over message
        boolean isOver = false, isScore = false;
        for (int i = 0; i < MAX_NUM_ATTEMPTS; i++) {
            Thread.sleep(DELAY); // Sleep to sync with game thread
            gameOutput = capturedOutput.toString().toUpperCase();
            isOver = gameOutput.contains(OVER_KEYWORD);
            isScore = gameOutput.contains(SCORE_KEYWORD);
            if (isOver && isScore) {
                break;
            }
        }

        // Assert that the game over and score messages are printed
        String msg_over = "Cannot find the message \"Game over\" in the standard output ";
        String msg_score = "Cannot find the message \"Your score is: X\" in the standard output ";
        if (totalWeight > MAX_TOTAL_WEIGHT) {
            String msg_weight_limit = "when weight limit is exceeded.";
            msg_over += msg_weight_limit;
            msg_score += msg_weight_limit;
        } else {
            String msg_spots_finished = "when all spots in the fishing pond are finished.";
            msg_over += msg_spots_finished;
            msg_score += msg_spots_finished;
        }
        assertTrue(msg_over, isOver);
        assertTrue(msg_score, isScore);

        // Assert that the total score printed by the game is correct
        Matcher scoreMatcher = scorePattern.matcher(gameOutput);
        assertTrue("Message \"Your score is: X\" was found but it contains an invalid value.", scoreMatcher.find());
        int score = Integer.parseInt(scoreMatcher.group(1));
        assertTrue("", totalScore == score);
    }

    @Test
    public void test1() throws InterruptedException {
        // Start a thread with the game
        gameThread.start();

        // Run tests
        String[] inputs = { "1\n1", "1\n2", "1\n3", "1\n4", "2\n1", "2\n2", "2\n3", "2\n4", "3\n1", "3\n2", "3\n3",
                "3\n4", "4\n1", "4\n2", "4\n3", "4\n4" };
        testFishingGame(inputs);

        // Close the game thread
        gameThread.interrupt();
    }

    @Test
    public void test2() throws InterruptedException {
        // Start a thread with the game
        gameThread.start();

        // Run tests
        String[] inputs = { "4\n4", "4\n3", "4\n2", "4\n1", "3\n4", "3\n3", "3\n2", "3\n1", "2\n4", "2\n3", "2\n2",
                "2\n1", "1\n4", "1\n3", "1\n2", "1\n1" };
        testFishingGame(inputs);

        // Close the game thread
        gameThread.interrupt();
    }
}