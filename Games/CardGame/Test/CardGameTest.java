package org.example;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class CardGameTest {

    // Set a timeout of 1 second for each test
    private static final int TIMEOUT_SECONDS = 1_000;

    @Test(timeout = TIMEOUT_SECONDS)
    public void testDealCard() {
        Deck<SuitedCard> deck = SuitedCard.getCompleteDeck();
        int completeSize = deck.size();
        deck.dealCard(); // Assuming a complete deck has at least 1 element
        assertEquals(completeSize, deck.size() + 1);
    }

    @Test(timeout = TIMEOUT_SECONDS)
    public void testAddCard() {
        Deck<SuitedCard> deck = SuitedCard.getCompleteDeck();
        int completeSize = deck.size();
        SuitedCard card = deck.dealCard(); // Assuming a complete deck has at least 1 element
        deck.addCard(card);
        assertEquals(completeSize, deck.size());
    }

    @Test(timeout = TIMEOUT_SECONDS)
    public void testAddMultipleCards() {
        Deck<SuitedCard> deck = SuitedCard.getCompleteDeck();
        int completeSize = deck.size();
        SuitedCard card1 = deck.dealCard();
        SuitedCard card2 = deck.dealCard(); // Assuming a complete deck has at least 2 elements
        deck.addCards(Arrays.asList(card1, card2));
        assertEquals(completeSize, deck.size());
    }

    @Test(timeout = TIMEOUT_SECONDS)
    public void testShuffle() {
        Deck<SuitedCard> deck = SuitedCard.getCompleteDeck();

        // Create a copy of the deck before shuffling
        List<ICard> deckCopy = new ArrayList<>(deck.getCards());

        deck.shuffle();

        // Decks should be different after shuffling
        // There's a 1 in 52! chance of a false positive :)
        assertNotEquals(deckCopy, deck.getCards());
    }

    @Test(timeout = TIMEOUT_SECONDS)
    public void testSort() {
        Deck<SuitedCard> deck = SuitedCard.getCompleteDeck();

        List<SuitedCard> cardsToSort = new ArrayList<>(deck.getCards());
        Collections.sort(cardsToSort);

        deck.sort();

        assertEquals(cardsToSort, deck.getCards());
    }

    @Test(timeout = TIMEOUT_SECONDS)
    public void testUnoCardDeck() {
        Deck<UnoCard> deck = UnoCard.getCompleteDeck();
        int completeSize = deck.size();
        UnoCard card = deck.dealCard(); // Assuming a complete deck has at least 1 element
        assertEquals(completeSize, deck.size() + 1);
        deck.addCard(card);
        assertEquals(completeSize, deck.size());
    }
}
