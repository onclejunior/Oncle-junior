package org.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck<T extends ICard> {
    private List<T> cards;

    public Deck() {
        this.cards = new ArrayList<>();
    }

    public void addCard(T card) {
        cards.add(card);
    }

    public void addCards(List<T> cards) {
        this.cards.addAll(cards);
    }

    public T dealCard() {
        if (cards.isEmpty()) {
            throw new IllegalStateException("No cards left in the deck");
        }
        return cards.remove(cards.size() - 1);
    }

    public List<T> getCards() {
        return new ArrayList<>(this.cards);
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    public void sort() {
        Collections.sort(cards);
    }

    public int size() {
        return cards.size();
    }

    @Override
    public String toString() {
        return "Deck of " + cards.size() + " cards";
    }
}
