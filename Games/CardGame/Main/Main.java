package org.example;

import org.example.SuitedCard.Rank;
import org.example.SuitedCard.Suit;
import org.example.UnoCard.UnoColor;
import org.example.UnoCard.UnoValue;

public class Main {
    public static void main(String[] args) {
        // Create and use a deck of suited cards (standard French deck)
        Deck<SuitedCard> suitedCardDeck = new Deck<>();
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                suitedCardDeck.addCard(new SuitedCard(suit, rank));
            }
        }

        suitedCardDeck.shuffle();
        System.out.println("Shuffled Suited Card Deck: " + suitedCardDeck);

        System.out.println("Dealing cards...");
        for (int i = 0; i < 5; i++) {
            System.out.println("Dealt card: " + suitedCardDeck.dealCard());
        }

        suitedCardDeck.sort();
        System.out.println("Sorted Suited Card Deck: " + suitedCardDeck);

        // Create and use a deck of Uno cards
        Deck<UnoCard> unoCardDeck = new Deck<>();
        for (UnoColor color : UnoColor.values()) {
            for (UnoValue value : UnoValue.values()) {
                unoCardDeck.addCard(new UnoCard(color, value));
            }
        }

        unoCardDeck.shuffle();
        System.out.println("Shuffled Uno Card Deck: " + unoCardDeck);

        System.out.println("Dealing cards...");
        for (int i = 0; i < 5; i++) {
            System.out.println("Dealt card: " + unoCardDeck.dealCard());
        }

        unoCardDeck.sort();
        System.out.println("Sorted Uno Card Deck: " + unoCardDeck);
    }
}
