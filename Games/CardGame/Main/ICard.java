package org.example;

public interface ICard extends Comparable<ICard> {
    // Return the description of the card, e.g. "Ace of Spades"
    public String getDescription();

    // Return the value of the card that determines its position after sorting
    public int getValue();
}