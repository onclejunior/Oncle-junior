package org.example;

import java.util.ArrayList;
import java.util.List;

public class SuitedCard implements ICard {
    public enum Suit {
        SPADES, HEARTS, CLUBS, DIAMONDS
    }

    public enum Rank {
        TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
    }

    private final Suit suit;
    private final Rank rank;

    public SuitedCard(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public static Deck<SuitedCard> getCompleteDeck() {
        List<SuitedCard> cards = new ArrayList<>();
        for (Suit s : Suit.values()) {
            for (Rank r : Rank.values()) {
                SuitedCard card = new SuitedCard(s, r);
                cards.add(card);
            }
        }
        Deck<SuitedCard> deck = new Deck<>();
        deck.addCards(cards);
        return deck;
    }

    public Suit getSuit() {
        return suit;
    }

    public Rank getRank() {
        return rank;
    }

    @Override
    public String getDescription() {
        return rank + " of " + suit;
    }

    @Override
    public int getValue() {
        // Calculate the value based on suit and rank
        return suit.ordinal() * Rank.values().length + rank.ordinal();
    }

    @Override
    public int compareTo(ICard other) {
        return Integer.compare(this.getValue(), other.getValue());
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
