package org.example;

import java.util.ArrayList;
import java.util.List;

public class UnoCard implements ICard {
    public enum UnoColor {
        RED, YELLOW, GREEN, BLUE
    }

    public enum UnoValue {
        ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, SKIP, REVERSE, DRAW_TWO
    }

    private final UnoColor color;
    private final UnoValue unoValue;

    public UnoCard(UnoColor color, UnoValue value) {
        this.color = color;
        this.unoValue = value;
    }

    public static Deck<UnoCard> getCompleteDeck() {
        List<UnoCard> cards = new ArrayList<>();
        for (UnoColor c : UnoColor.values()) {
            for (UnoValue v : UnoValue.values()) {
                UnoCard card = new UnoCard(c, v);
                cards.add(card);
            }
        }
        Deck<UnoCard> deck = new Deck<>();
        deck.addCards(cards);
        return deck;
    }

    public UnoColor getColor() {
        return color;
    }

    public UnoValue getUnoValue() {
        return unoValue;
    }

    @Override
    public String getDescription() {
        return unoValue + " of " + color;
    }

    @Override
    public int getValue() {
        // Calculate the value based on color and value
        return color.ordinal() * UnoValue.values().length + unoValue.ordinal();
    }

    @Override
    public int compareTo(ICard other) {
        return Integer.compare(this.getValue(), other.getValue());
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
